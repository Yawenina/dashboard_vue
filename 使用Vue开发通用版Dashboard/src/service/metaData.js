﻿define(['service/resource'], function ($resource) {
    
    var metaDataService = function () {
        ////保存全部数据
        //this.data = {};
        ////保存维度数据
        //this.dimension = {};
        ////保存指标数据
        //this.indicator = {};
    }

    /**
     * 根据指定的一组数据源选项，获取metaData数据
     * @param widget 当前组件
     * @returns {JQuery.Deferred} ajax数据请求的deferred对象
     */
    metaDataService.prototype.requestAll = function (widget, needAddOptionPairs) {
        var param = {
            "productId": widget.productId,
            "solutionId": widget.solutionId,
            "profileId": widget.profileId,
            "apiType": widget.apiType,
            "queryEntity": {
                "cultureName": "zh-CN",
                "options": {
                    "optionPairs": [
                      {
                          "key": "ProfileId",
                          "value": widget.profileId
                      }]
                }
            }
        }

        //获取MetaData需要添加的特殊选项
        if (needAddOptionPairs.length > 0) {
            var options = param.queryEntity.options;
            options.optionPairs = options.optionPairs.concat(needAddOptionPairs);
        }

        return $resource.save({ module: 'request', action: 'GroupedMetaData' }, param).then(function (response) {
            return response.data.Result;
        });
    }

    return new metaDataService();
})