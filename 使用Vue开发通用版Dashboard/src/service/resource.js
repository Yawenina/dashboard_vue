﻿define(['vue-resource'], function (VueResource) {

    var ResourceService = function () {

        var serverIP = 'http://beta-dashboard.gridsumdissector.com';
        var serverIP = 'http://dashboard-beta.gridsumdissector.com';
        var serverIP = '';

        if (window.location.href.indexOf('localhost') > -1) {
            serverIP = 'http://localhost:6896';
        }
        var requestUrl = serverIP + '/api{/module}{/action}{/id}';
        this.$resource = Vue.resource(requestUrl);        
    }

    /**
     * 发送请求
     * @param ajaxType 请求类型（'get', 'save', 'update', 'delete'）
     * @param parameters 发送请求需要的参数
     * @returns {jQuery.Deferred} jQuery的Deferred对象
     */
    ResourceService.prototype.ajax = function (ajaxType, parameters) {
        var deferred = $.Deferred();
        //因为传递过来的parameters一个数组对象，所以使用了apply，将数组对象变成了多个参数
        this.$resource[ajaxType].apply(this.$resource, parameters).then(
            function (response) {
                this.resolve(response);
            }.bind(deferred),
            function (response) {
                this.reject(response);
            }.bind(deferred));

        return deferred.promise();
    }

    ResourceService.prototype.get = function() {
        return this.ajax('get', arguments);
    }

    ResourceService.prototype.save = function () {
        return this.ajax('save', arguments);
    }

    ResourceService.prototype.update = function () {
        return this.ajax('update', arguments);
    }

    ResourceService.prototype.delete = function () {
        return this.ajax('delete', arguments);
    }

    return new ResourceService();
})