/**
 * 继承通用ComponentService，给全局dashboardModule扩展了一些方法
 */
define(['service/component', 'model/model', 'model/constructor/Dashboard'], function (ComponentService, Model, Dashboard) {

    if (!Model.dashboard) {

        var DashboardService = function () {
            ComponentService.call(this);
        }

        DashboardService.prototype = new ComponentService();

        /**
         * 发送获取指定分组下的仪表盘请求
         * @param groupId 分组ID
         * @param callback 发送请求的回调函数
         * @returns {jQuery.Deferred} Ajax请求返回的Deferred对象
         */
        DashboardService.prototype.sendRequest = function(groupId, callback) {
            var dashboardObj = new Dashboard(groupId);
            return dashboardObj.getAll().then(function(response) {
                this.callback(response.data, this.dashboardObj);
            }.bind({
                dashboardObj: dashboardObj,
                callback: callback
            }));
        }

        /**
         * 获取指定分组下的全部仪表盘
         * @param groupId 分组ID
         * @return {Promise} 请求并处理完后的promise
         */
        DashboardService.prototype.requestAll = function (groupId) {
            return this.sendRequest(groupId, function (response, dashboardObj) {
                var dashboardServ = this.context;
                dashboardObj.addResponseDataToServObj(response, dashboardServ);

                //对已添加的所有组件按照名称升序排序
                dashboardServ.orderBy('name');

                //从Config中获取默认选中的组件
                if (this.group.config && this.group.config.defaultSelected > 0) {
                    dashboardServ.setById(this.group.config.defaultSelected);
                } else {
                    //设置排序后的第一个为当前组件
                    dashboardServ.set(0);
                }

                return dashboardServ.getAll();
            }.bind({
                context: this,                
                group: Model.group.getById(groupId)
            }));
        }

        /**
         * 创建一个新的空白仪表盘
         * @param groupId 分组Id
         */
        DashboardService.prototype.createNew = function (groupId) {
            return new Dashboard(groupId);
        }

        Model.dashboard = new DashboardService();
    }

    return Model.dashboard;
});
