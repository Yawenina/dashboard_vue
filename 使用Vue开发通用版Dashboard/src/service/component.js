/**
 * 定义所有组件（Group、Dashboard、Widget）提供的服务方法
 */
define([], function () {

    var ComponentService = function () {

        this.components = [];
        this.component = null;
        this.componentIndex = -1;
    }

    ComponentService.prototype = {
        /**
         * 获取当前索引
         * @returns {Number} 索引值
         */
        getIndex: function () {
            return this.componentIndex;
        },

        /**
         * 根据id获取所在的索引位置
         * @param id componentId
         * @returns {Number} 索引值
         */
        getIndexById: function (id) {
            for (var i = 0; i < this.components.length; i++) {
                if (this.components[i].id.toString() === id.toString()) {
                    return i;
                }
            }
            return -1;
        },

        /**
         * 获取指定索引的组件，如果未提供索引，则获取当前组件
         * @param index 组件索引
         * @returns {Model.BaseModel} 获取的组件
         */
        get: function (index) {
            if (this.components.length > index && index > -1) {
                return this.components[index];
            } else {
                return this.components[this.componentIndex];
            }
        },

        /**
         * 根据id获取组件对象
         * @param id 组件ID
         * @returns {Model.BaseModel} 组件对象，如果未找到，则返回null
         */
        getById: function (id) {
            var index = this.getIndexById(id);
            if (-1 < index) {
                return this.get(index);
            }
            return null;
        },

        /**
         * 获取全部组件
         * @returns {Array<Model.BaseModel>} 全部组件
         */
        getAll: function () {
            return this.components;
        },

        /**
         * 设置第几个组件为当前组件
         * @param index 组件索引
         */
        set: function (index) {
            if (this.components.length > index && index > -1) {
                this.componentIndex = index;
                this.component = this.components[this.componentIndex];
            }
        },

        /**
         * 根据组件ID，设置哪个组件为当前组件
         * @param id 组件Id
         */
        setById: function (id) {
            this.set(this.getIndexById(id));
        },

        /**
         * 添加一个组件
         * @param component 要添加的组件
         */
        add: function (component) {
            if (!component || component.id < 0) {
                return;
            }
            this.componentIndex = this.components.length;
            this.components.push(component);
            this.component = this.components[this.componentIndex];
        },

        /**
         * 删除一个组件，如果未提供，则表示要删除当前组件
         * @param index 组件索引
         */
        del: function (index) {
            if (typeof index !== "number") {
                index = this.componentIndex;
            }
            if (this.components.length <= index || index < 0) {
                return;
            }

            this.components.splice(index, 1);
            if (this.components.length === 0) {
                this.componentIndex = -1;
                this.component = null;
            } else {
                this.componentIndex = Math.max(0, this.componentIndex - 1);
                this.component = this.components[this.componentIndex];
            }
        },

        /**
         * 删除一个指定id的组件
         * @param id 组件ID
         */
        delById: function(id) {
            var index = this.getIndexById(id);
            this.del(index);
        },

        /**
         * 删除所有组件
         */
        delAll: function () {
            /**
             * 因为 JavaScript 的限制，Vue.js 不能检测到下面数组变化：
             * 1、直接用索引设置元素，如 vm.items[0] = {}——解决方案：使用Vuejs提供的扩展数组方法:$set()
             * 2、修改数据的长度，如 vm.items.length = 0——解决方案：赋给一个空数组
             */
            this.components = [];
            this.componentIndex = -1;
            this.component = null;
        },

        /**
         * 对已添加的组件按照指定属性进行排序
         * @param propName 属性名
         * @param isDescOrder 是否降序排序（默认值：升序）
         */
        orderBy: function (propName, isDescOrder) {
            this.components.sort(function (first, second) {
                if (first[propName] > second[propName]) {
                    return isDescOrder === true ? -1 : 1;
                }
                return isDescOrder === true ? 1 : -1;
            });
        }
    }

    return ComponentService;
});