define(['model/model', 'service/resource'], function (Model, $resource) {

    if (!Model.datasource) {


        var DatasourceService = function (data) {
            this.data = data;
            this.selected = 0;
        }

        DatasourceService.prototype.constructor = DatasourceService;

        /**
         * 获取全部产品列表
         * @returns {Array<JSON>} 全部产品列表 
         */
        DatasourceService.prototype.getAll = function () {
            return this.data;
        }

        /**
         * 获取指定id的产品列表
         * @param id 要获取的某一个id值或者id数组
         * @return {JSON} 指定id的产品列表
         */
        DatasourceService.prototype.getById = function (id) {

            if (!(id instanceof Array)) {
                return this.data[id];
            }

            var result = {};
            for (var i = 0; i < id.length; i++) {
                var obj = this.data[id[i]];
                if (obj) {
                    result[id[i]] = obj;
                }
            }
            return result;
        }

        /**
         * 获取指定id的下一级id集合
         * @param id 要获取的某一个id值或者id数组
         * @return {Array<Number>} 下一级id集合
         */
        DatasourceService.prototype.getNextLevelById = function (id) {
            var datas = this.getById(id);

            //如果指定的id是一个，那么根据id获取的数据项也是一个，直接返回nextLevelId即可
            if (!(id instanceof Array)) {
                return datas.nextLevelId;
            }

            var result = [];
            for (var item in datas) {
                var obj = datas[item];
                result = result.concat(obj.nextLevelId);
            }
            return result;
        }

        Model.datasource = {
            /**
             * 获取全部产品列表
             * @param groupId 分组ID
             * @return {jQuery.Deferred} 请求并处理完后的jQuery.Deferred对象
             */
            requestAll: function () {

                return $resource.get({ module: 'product', action: 'getAllProfiles' }).then(function (response) {

                    var convertedData = this.context.dataConvert(response.data.Result);

                    for (var propName in convertedData) {
                        this.context[propName] = new this.DatasourceServiceClass(convertedData[propName]);
                        //@todo 因为后台服务提供获取apiType的接口需要再发一个GetAllProducts请求，暂时先写在这儿
                        if (propName === 'product') {
                            for (var productId in convertedData[propName]) {
                                switch (productId) {
                                    case '1024':
                                        this.context[propName].data[productId].apiType = {
                                            1: {
                                                name: '普通报表'
                                            },
                                            2: {
                                                name: '频次报表'
                                            },
                                            //3: {
                                            //    name: '重合度报表'
                                            //},
                                            //4: {
                                            //    name: '独占报表'
                                            //}
                                        };
                                        break;
                                    default:
                                        this.context[propName].data[productId].apiType = {
                                            1: {
                                                name: '普通报表'
                                            }
                                        };
                                        break;
                                }
                            }
                        }
                    }
                }.bind({
                    context: this,
                    DatasourceServiceClass: DatasourceService
                }))
            },
            /**
             * 将返回的数据转变成以id作为key的格式
             * @param responseData 请求返回的数据
             * @return {JSON} 转换后的数据
             */
            dataConvert: function (responseData) {
                var returnObj = {};
                fnConvertNode(responseData, '');
                return returnObj;

                /**
                 * 递归转换一组节点
                 * @param nodes 节点集合
                 * @param higherLevelId 上一级的id值
                 * @return {Array<Number>} 所有子节点Id集合
                 */
                function fnConvertNode(nodes, higherLevelId) {
                    var idList = [];

                    for (var i = 0; i < nodes.length; i++) {

                        var node = nodes[i];

                        for (var propName in node) {
                            //因为服务器上返回的属性名第一个字母大写，所以要转换成小写
                            var newPropName = propName.replace(/\w/, function (char) {
                                return char.toLowerCase();
                            });
                            var newPropValue = node[propName];
                            if (propName === "NodeType") {
                                //因为服务器上返回的nodeType值第一个字母大写，所以要转换成小写
                                newPropValue = newPropValue.replace(/\w/, function (char) {
                                    return char.toLowerCase();
                                });
                            }
                            node[newPropName] = newPropValue;
                            delete node[propName];
                        }

                        var nodeType = node.nodeType;
                        if (!returnObj[nodeType]) {
                            returnObj[nodeType] = {};
                        }

                        var nodeList = returnObj[nodeType];

                        //因为不同product下solution或profile的id有重复，所以key需要包含上一级id
                        var id = higherLevelId + '' + node.nodeId,
                            name = node.nodeName;
                        nodeList[id] = {
                            id: node.nodeId,    //这里保存真正的id值，以便请求时使用
                            name: name
                        };

                        if (node.children instanceof Array && node.children.length > 0) {
                            var nextLevelId = fnConvertNode(node.children, id);
                            //所有子节点的nodeType都是一样的，所以取了第一个子节点的NodeType值
                            var nextLevel = node.children[0].nodeType;

                            nodeList[id].nextLevel = nextLevel;
                            nodeList[id].nextLevelId = nextLevelId;
                        }

                        idList.push(id);
                    }
                    return idList;
                }
            },

            /**
             * 获取数据源配置界面所需要的特殊参数
             * @returns {} 
             */
            requestAccountList: function (commonQueryParam, paramId) {
                commonQueryParam.paramId = paramId;

                return $resource.save({ module: 'request', action: 'params' }, JSON.stringify([commonQueryParam])).then(function(response) {
                    
                    if (response.data.Status !== 200) {
                        return [];
                    }

                    //因为查询的是查某一个AccountType的推广账户或自定义分组，所以直接取第一个节点即可
                    var result = response.data.Result[0][0];                                        
                    return result;
                });
            }
        };


    }

    return Model.datasource;
});
