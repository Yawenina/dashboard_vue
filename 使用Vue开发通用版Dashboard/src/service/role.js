﻿define(['service/component', 'model/constructor/role'], function (ComponentService, Role) {

    var RoleService = function () {
        ComponentService.call(this);
    }

    RoleService.prototype = new ComponentService();

    var _tempRoleId = 1;
    /**
     * 创建一个新的用户角色
     * @param roleName 角色名称
     * @returns 新建的角色对象 
     */
    RoleService.prototype.createNew = function (roleName) {
        var role = new Role();
        role.id = _tempRoleId++;
        role.name = roleName;
        role.status = 'create';
        role.isCurrent = false;
        role.isSelected = false;
        return role;
    }

    /**
     * 删除用户角色
     * @returns {jQuery.Deferred} Ajax请求返回并处理后的Deferred对象 
     */
    RoleService.prototype.delete = function () {
        var params = [],
            roles = this.getAll();

        for (var i = 0; i < roles.length; i++) {
            var role = roles[i];
            if (role.status === 'delete') {
                params.push(role.id);
            }
        }

        if (params.length > 0) {
            return new Role().delete(params).then(function (response) {
                if (response.data.Status === 200) {
                    for (var i = 0; i < this.delRole.length; i++) {
                        this.context.delById(this.delRole[i]);
                    }

                    return this.delRole;
                }
                return response.data;
            }.bind({
                context: this,
                delRole: params
            }));
        } else {
            return $.Deferred().resolve();
        }
    }

    /**
     * 修改用户角色
     * @returns {jQuery.Deferred} Ajax请求返回并处理后的Deferred对象 
     */
    RoleService.prototype.save = function () {
        var params_add = [],
            params_update = [],
            effectRole = {
                add: [],
                update: []
            },
        roles = this.getAll();

        for (var i = 0; i < roles.length; i++) {
            var role = roles[i];
            if (role.status === 'create') {
                params_add.push({
                    name: role.name,
                    privilege: role.privilege
                });
                effectRole.add.push(role);
            }
            else if (role.status === 'update') {
                params_update.push({
                    id: role.id,
                    name: role.name,
                    privilege: role.privilege
                });
                effectRole.update.push(role);
            }
        }

        var deferred = [],
            roleObj = new Role();

        if (params_add.length > 0) {
            deferred.push(roleObj.save(true, params_add));
        }
        if (params_update.length > 0) {
            deferred.push(roleObj.save(false, params_update));
        }

        if (deferred.length === 0) {
            return $.Deferred().resolve();
        } else {
            return $.when.apply($, deferred).then(function () {
                var errorResponse = [];
                for (var i = 0; i < arguments.length; i++) {
                    var response = arguments[i].data;
                    if (response.Status === 200) {
                        if (response.Result instanceof Array) {
                            var addRole = this.effectRole.add;
                            for (var j = 0; j < addRole.length; j++) {
                                var role = this.context.getById(addRole[j].id);
                                role.id = response.Result[j];
                                role.status = 'noModify';
                            }
                        } else {
                            var updateRole = this.effectRole.update;
                            for (var j = 0; j < updateRole.length; j++) {
                                this.context.getById(updateRole[j].id).status = 'noModify';
                            }
                        }
                    } else {
                        errorResponse.push(response);
                    }
                }
                return errorResponse;
            }.bind({
                context: this,
                effectRole: effectRole
            }));
        }
    }

    /**
     * 获取全部用户角色
     * @returns {jQuery.Deferred} Ajax请求返回并处理后的Deferred对象
     */
    RoleService.prototype.requestAll = function () {
        var role = new Role();
        return role.getAll().then(function (response) {
            var roleServ = this.context,
                roleObj = this.roleObj;

            /**
             * 因为需要附加额外的属性，并让Vuejs进行监测，所以没有直接调用addResponseDataToServObj方法
             * 而是转换完成后手动添加
             */
            roleObj.convertDataToList(response.data).forEach(function (role) {
                role.isCurrent = false;
                role.isSelected = false;
                this.add(role);
            }.bind(roleServ));

            var roles = roleServ.getAll();
            //按照名称升序排序
            roles.sort(function (first, second) {
                if (first.name.localeCompare(second.name) > 0) {
                    return 1;
                }
                return -1;
            });

            //修改status
            for (var i = 0; i < roles.length; i++) {
                roles[i].status = 'noModify';
            }

            return roles;
        }.bind({
            context: this,
            roleObj: role
        }));
    }

    return new RoleService();
})