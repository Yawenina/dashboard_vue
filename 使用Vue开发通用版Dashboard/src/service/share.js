﻿/**
 * 继承通用ComponentService，给全局GroupModule扩展了一些方法
 */
define(['service/component', 'model/model', 'model/constructor/ShareUser'],
    function (ComponentService, Model, ShareUser) {

        if (Model.share) {
            return Model.share;
        }

        var ShareService = function () {
            ComponentService.call(this);
        };

        ShareService.prototype = new ComponentService();
        ShareService.prototype.constructor = ShareService;

        /**
         * 获取指定仪表盘的共享用户         
         * @param dashboardId 指定的仪表盘
         * @return {jQuery.Deferred} Ajax请求并处理完后的Deferred对象
         */
        ShareService.prototype.requestAll = function (dashboardId) {

            var shareUser = new ShareUser(dashboardId);

            return shareUser.getAll().then(function (response) {
                var shareServ = this.context;
                this.shareUserObj.addResponseDataToServObj(response.data, shareServ);                

                return shareServ.getAll();
            }.bind({
                context: this,
                shareUserObj: shareUser
            }));
        }

        /**
         * 获取全部可用的共享权限
         * @return {jQuery.Deferred} Ajax请求并处理完后的Deferred对象
         */
        ShareService.prototype.requestAllPermission = function () {

            var shareUser = new ShareUser();

            return shareUser.getAllPermission().then(function (response) {
                var permissions = this.shareUserObj.convertDataToList(response.data);                
                return permissions;
            }.bind({
                shareUserObj: shareUser
            }));
        }

        /**
         * 新建一个共享用户对象
         * @returns {Model.ShareUser} 共享用户对象
         */
        ShareService.prototype.createNew = function (dashboardId, email, permissionId) {
            return new ShareUser(dashboardId, email, permissionId);
        }

        /**
         * 删除一个共享用户
         * @param {String} id 共享用户的关键字
         */
        ShareService.prototype.delUser = function (id) {            
            this.delById(id);
        }

        /**
         * 保存对当前仪表盘的所有共享操作
         * @return {jQuery.Deferred} Ajax请求并处理完后的Deferred对象 
         */
        ShareService.prototype.save = function() {
            var shareUsers = this.getAll();
            if (shareUsers.length === 0) {
                return $.Deferred().resolve('没有共享用户');
            }

            return shareUsers[0].save(shareUsers).then(function (response) {
                if (response.data.Status === 200) {
                    return response.data.Result;
                }

                return response.data;
            });
        }

        Model.share = new ShareService();
        return Model.share;
    })