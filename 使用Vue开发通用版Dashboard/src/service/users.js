﻿define(['service/component', 'model/model', 'model/constructor/UserInfo', 'model/constructor/Role'],
    function (ComponentService, Model, UserInfo, Role) {

        if (Model.users) {
            return Model.users;
        }

        var UsersService = function () {
            ComponentService.call(this);
        }

        UsersService.prototype = new ComponentService();

        /**
         * 获取全部用户
         * @returns {jQuery.Deferred} Ajax请求返回并处理后的Deferred对象
         */
        UsersService.prototype.requestAll = function () {
            var userInfoObj = new UserInfo();
            return userInfoObj.getAll().then(function (response) {
                var usersServ = this.context;
                this.userInfoObj.addResponseDataToServObj(response.data, usersServ);

                var users = usersServ.getAll();
                //修改status
                for (var i = 0; i < users.length; i++) {
                    users[i].role.status = 'noModify';
                }

                return users;
            }.bind({
                context: this,
                userInfoObj: userInfoObj
            }));
        }

        /**
         * 获取全部角色
         * @returns {jQuery.Deferred} Ajax请求返回并处理后的Deferred对象
         */
        UsersService.prototype.requestAllRole = function () {
            var roleObj = new Role();
            return roleObj.getAll().then(function (response) {
                return this.convertDataToList(response.data);
            }.bind(roleObj));
        }

        /**
         * 保存多个用户角色
         * @param needSaveUsers 用户id与角色id之间的映射
         * @returns {jQuery.Deferred} Ajax请求返回并处理后的Deferred对象
         */
        UsersService.prototype.saveUserAndRoleMap = function (needSaveUsers) {
            var userInfoObj = new UserInfo();
            return userInfoObj.saveRole(needSaveUsers).then(function (response) {
                return response.data;
            });
        }

        Model.users = new UsersService();
        return Model.users;
    }
)