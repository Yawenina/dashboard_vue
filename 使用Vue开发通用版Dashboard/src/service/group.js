/**
 * 继承通用ComponentService，给全局GroupModule扩展了一些方法
 */
define(['service/component', 'model/model', 'model/constructor/Group'], function (ComponentService, Model, Group) {

    if (!Model.group) {

        var GroupService = function () {
            ComponentService.call(this);
        }

        GroupService.prototype = new ComponentService();

        /**
         * 获取全部分组
         * @return {Promise} 请求并处理完后的promise
         */
        GroupService.prototype.requestAll = function () {
            var groupObj = new Group();
            return groupObj.getAll().then(function (response) {
                var groupServ = this.context;
                this.groupObj.addResponseDataToServObj(response.data, groupServ);

                //删除上海大众分组
                groupServ.delById(10008);

                //对已添加的所有组件按照名称升序排序
                this.context.orderBy('name');

                //从Config中获取默认选中的组件
                if (this.userInfo.config && this.userInfo.config.defaultSelected > 0) {
                    this.context.setById(this.userInfo.config.defaultSelected);
                }
                else{
                    //设置排序后的第一个为当前组件
                    this.context.set(0);
                }

                return groupServ.getAll();
            }.bind({
                context: this,
                groupObj: groupObj,
                userInfo: Model.userInfo
            }))
        }

        /**
         * 创建一个新的空白仪表盘分组
         */
        GroupService.prototype.createNew = function () {
            return new Group();
        }

        Model.group = new GroupService();
    }

    return Model.group;
});
