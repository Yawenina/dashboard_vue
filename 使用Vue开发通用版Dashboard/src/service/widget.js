/**
 * 继承通用ComponentService，给全局widgetModule扩展了一些方法
 */
define(['service/component', 'model/model', 'model/constructor/WidgetExtend'], function (ComponentService, Model, Widget) {

    if (!Model.widget) {

        var customWidgetId = 1;

        var WidgetService = function () {
            ComponentService.call(this);
            this.charts = [];
            this.filters = [];
        }

        WidgetService.prototype = new ComponentService();

        /**
         * 重写父类的add方法，增加记录widget类型功能。此方法会在requestAll()中addResponseDataToServObj()调用
         * @param widget 要添加的widget         
         */
        WidgetService.prototype.add = function (widget) {
            ComponentService.prototype.add.call(this, widget);

            if (widget.chartType < 20000000) {  //是否是图表
                this.charts.push(widget);
            }
            else if (widget.chartType < 30000000) { //是否是过滤器
                this.filters.push(widget);
            }
        }

        /**
         * 根据类型获取widget
         * @param type 类型名称（chart、filter）
         * @returns {Array<Widget>} 指定类型的widget集合
         */
        WidgetService.prototype.getByType = function (type) {
            switch (type) {
                case 'filter':
                    return this.filters;
                default:
                    return this.charts;
            }
        }

        /**
         * 获取指定仪表盘下的全部小部件
         * @param dashboardId 仪表盘ID
         * @return {jQuery.Deferred} 请求并处理完后的jQuery.Deferred对象
         */
        WidgetService.prototype.requestAll = function (dashboardId) {
            /**
             * 每次dashboard-show的时候应该重新获取一次allWidget，因为有不保存新建或者修改widget的情况
             */
            ////如果是仪表盘id未发生变化，则直接返回当前的所有widget
            //if ( this.getAll().length > 0 && this.get().dashboardId === dashboardId) {
            //    var deferred = $.Deferred();
            //    deferred.resolve(this.getAll());
            //    return deferred;
            //}

            var widgetObj = new Widget(dashboardId);
            return widgetObj.getAll().then(function (response) {
                var widgetServ = this.context;
                this.widgetObj.addResponseDataToServObj(response.data, widgetServ);

                var widgets = widgetServ.getAll();
                //修改widget的status
                for (var i = 0; i < widgets.length; i++) {
                    var widget = widgetServ.get(i);
                    widget.config.status = 'noModify';                    

                    /**
                     * @todo 因为上海大众的product值（AD）是1，而服务器端返回的AD是1024，为了兼容，临时修改productID
                     */
                    if (widget.productId === 1) {
                        widget.productId = 1024;
                    }
                }

                return widgets;
            }.bind({
                context: this,
                widgetObj: widgetObj,
                dashboard: Model.dashboard.getById(dashboardId)
            }))
        }

        /**
         * 创建一个新的空白小部件
         * @param dashboardId 仪表盘Id
         */
        WidgetService.prototype.createNew = function (dashboardId) {
            var widget = new Widget(dashboardId);
            //临时设置一个widget的id，方便后面的查找
            widget.id = customWidgetId++;
            widget.config.status = 'create';
            return widget;
        }        

        Model.widget = new WidgetService();
    }

    return Model.widget;
});
