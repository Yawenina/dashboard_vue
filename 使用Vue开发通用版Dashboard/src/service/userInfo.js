﻿define(['model/model', 'model/constructor/UserInfo', 'model/constructor/UserValidate'],
    function (Model, UserInfo, UserValidate) {

        if (!Model.userInfo) {

            var UserInfoService = function () {
                UserInfo.call(this);
                this.userValidateObj = new UserValidate();
            }

            UserInfoService.prototype = new UserInfo();

            /**
             * 获取用户信息
             * @param fnUserReadyCallback 用户信息获取完成后的回调方法
             */
            UserInfoService.prototype.get = function (fnUserReadyCallback) {                

                this.userValidateObj.start().then(function (response) {
                    var result = response.data.Result;
                    if (result) {
                        this.context.prototypeCopy(result);
                        //给所有Ajax请求增加Authorization
                        Vue.http.headers.common['Authorization'] = encodeURIComponent(this.context.token);

                        if (typeof this.callback === 'function') {
                            this.callback();
                        }
                    }
                    else {
                        console.debug(response.data.Message);
                        //this.context.userValidateObj.jumpToLogin();
                    }

                }.bind({
                    context: this,
                    callback: fnUserReadyCallback
                })
                );
            }

            /**
             * 退出系统
             */
            UserInfoService.prototype.logout = function () {
                if (this.token.length === 0) {
                    return;
                }

                this.userValidateObj.logout(this.token);

            }            

            Model.userInfo = new UserInfoService();
        }

        return Model.userInfo;
    })