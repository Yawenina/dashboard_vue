define(['model/model',
    'availableWidget/曲线图', 'availableWidget/柱状图', 'availableWidget/饼状图', 'availableWidget/日期范围'
], function (Model) {

    if (!Model.availableWidget) {
        var availableWidget = arguments;

        var AvailableWidgetService = function () {
            this.widgets = [];
            var needLoadChartLibrary = [];
            //arguments的第一个参数的Model，不是可用的availableWidget，所以i从1开始
            for (var i = 1; i < availableWidget.length; i++) {
                var widget = availableWidget[i];
                if (typeof widget.order !== 'number') {
                    widget.order = i;
                }
                this.widgets.push(widget);
                needLoadChartLibrary.push(widget.chartLibrary);
            }
        }

        /**
         * 获取全部可用的小部件列表
         * @return {Array<component.availableWidget} 可用的组件列表
         */
        AvailableWidgetService.prototype.getAll = function () {
            this.widgets.sort(function (first, second) {
                if (first.order > second.order) {
                    return 1;
                }
                return -1;
            });

            return this.widgets;
        }

        /**
         * 获取指定name的availableWidget
         * @param name 可用小部件的name
         */
        AvailableWidgetService.prototype.getByName = function (name) {
            for (var i = 0; i < this.widgets.length; i++) {
                var widget = this.widgets[i];
                if (widget.name === name) {
                    return widget;
                }
            }
        }

        Model.availableWidget = new AvailableWidgetService();
    }


    return Model.availableWidget;
});
