﻿(function () {

    require.config({
        paths: {
            jquery: '../scripts/jquery-2.1.4.min',
            'jquery-ui': '../scripts/jquery-ui-1.11.4.min',
            gridstack: '../scripts/gridstack.min',
            lodash: '../scripts/lodash.min',
            bootstrap: '../scripts/bootstrap.min',
            moment: '../scripts/moment.min',
            daterangepicker: '../scripts/daterangepicker',
            vue: '../scripts/vue',
            'vue-resource': '../scripts/vue-resource',
            echarts: '../scripts/echarts/echarts.min',

            text: '../scripts/require-text',        //requirejs的插件text.js必须配制成text作为key

            availableWidget: 'component/availableWidget',
            component: 'component',
            model: 'model',
            service: 'service',
            template: 'template'
        },        
        shim: {
            bootstrap: ['jquery'],
            template: ['require-text']
        }
    });

    require(['vue', 'vue-resource', 'echarts', 'bootstrap', 'gridstack', 'daterangepicker'], function (Vue, VueResource) {

        window.Vue = Vue;
        Vue.use(VueResource);

        require(['service/userInfo', 'component/appTop', 'component/appLeft', 'component/appContent', 'component/logo' ],
            function (userInfoServ, appTopComp, appLeftComp, appContentComp, logoComp) {

                userInfoServ.get(function () {
                    var vue = new Vue({
                        el: '#app',
                        components: {
                            'app-top': appTopComp,
                            'app-left': appLeftComp,
                            'app-content': appContentComp,
                            logo: logoComp
                        },
                        events: {
                            /**
                             * 子组件会通过冒泡的方式将事件传递过来，然后再通过广播的方式传递下去，这样来实现同级组件之间的事件传递
                             */
                            'callSiblingEvent': function () {
                                this.$broadcast.apply(this, arguments);
                            }
                        },
                        ready: function () {
                            //通知用户信息获取完成
                            this.$broadcast('userReady');
                        }
                    });
                });
            }
        )
    });
})()