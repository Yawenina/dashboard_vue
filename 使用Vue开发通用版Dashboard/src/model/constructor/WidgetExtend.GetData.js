﻿define(["model/constructor/Widget"], function (Widget) {

    var WidgetExtend = function () {
        Widget.apply(this, arguments);
    }

    WidgetExtend.prototype = new Widget;
    WidgetExtend.prototype.constructor = WidgetExtend;

    /**
	 * 刷新当前widget的数据
	 * @param selectFilterItem 选择的过滤项
	 * @returns {jQuery.Deferred} 处理后的数据
	 */
    WidgetExtend.prototype.refreshData = function (selectFilterItem) {

        return $.when.apply($, this.getData(selectFilterItem)).then(function () {
            var _this = this.context;

            var availableWidget = require('service/availableWidget').getByName(_this.config.widgetName);

            //往传递的参数中插入当前widget
            Array.prototype.unshift.call(arguments, _this);
            //对获取到的数据进行处理
            var data = availableWidget.processData.apply(availableWidget, arguments);

            //绑定数据到界面
            availableWidget.bindData(_this, data);

            return data;
        }.bind({
            context: this
        }));
    }

    /**
     * 获取所有数据源的数据
     * @param selectFilterItem 选择的过滤项
     * @returns {Array<jQuery.Deferred>} jQuery的Deferred对象集合
     */
    WidgetExtend.prototype.getData = function (selectFilterItem) {
        var deferreds = [];
        //@todo 目前一个widget只配置了一个数据源，如果是多个数据源，需要在这个方法里处理
        deferreds.push(this.request(selectFilterItem));

        return deferreds;
    }

    /**
     * 获取一个数据源的数据
     * @param selectFilterItem 选择的过滤项
     * @returns {jQuery.Deferred} jQuery的Deferred对象
     */
    WidgetExtend.prototype.request = function (selectFilterItem) {

        var param = this.dsConvertToParam(selectFilterItem);

        if (!param) {
            return $.Deferred().resolve();
        }

        //后台数据请求要求post方式，save方法用的是post方式
        return this.$resource.save({ module: 'request', action: 'result' }, param).then(function (response) {
            return response.data.Result;
        });
    }

    /**
     * 将数据源转换成请求的参数
     * @param selectFilterItem 选择的过滤项
     * @returns {JSON} 符合要求的请求参数
     */
    WidgetExtend.prototype.dsConvertToParam = function (selectFilterItem) {

        //验证参数，如果不正确，则返回null
        if (this.productId < 1 ||
            this.solutionId < 1 ||
            this.profileId < 1 ||
            this.apiType < 1 ||
            (this.config.selectDimension.length === 0 && this.config.selectIndicator.length === 0)) {

            return null;
        }

        var param = {
            "productId": this.productId,
            "solutionId": this.solutionId,
            "profileId": this.profileId,
            "apiType": this.apiType,
            "queryEntity": {
                "options": {
                    "optionPairs": [
                        {
                            "key": "ProfileId",
                            "value": this.profileId
                        }
                    ]
                },
                "groupBys": [],
                "orderBys": [],
                "paging": {}
            }
        };

        var queryEntity = param.queryEntity;

        var columns = [];
        for (var i = 0; i < this.config.selectDimension.length; i++) {
            columns.push(this.config.selectDimension[i].key);
        }
        for (var i = 0; i < this.config.selectIndicator.length; i++) {
            columns.push(this.config.selectIndicator[i].key);
        }
        queryEntity.columns = columns;


        var filters = [];
        for (var i = 0; i < this.config.selectFilterCondition.length; i++) {
            var filterCondition = this.config.selectFilterCondition[i];
            filters.push({
                "filterItemType": "single",
                "filterOn": filterCondition.key,
                "filterType": filterCondition.type,
                "filterValues": filterCondition.value.split(',')    //要考虑in的情况，界面是文本框，服务器需要的是数组，所以split
            });
        }

        //widget配置的过滤条件与过滤器两者暂时按照and进行叠加处理
        for (var filterKey in selectFilterItem) {
            var filterItem = selectFilterItem[filterKey];
            switch (filterKey) {
                case 'startDate':
                    queryEntity.options.optionPairs.push({
                        "key": "StartTime",
                        "value": filterItem.value
                    });
                    break;
                case 'endDate':
                    queryEntity.options.optionPairs.push({
                        "key": "EndTime",
                        "value": filterItem.value
                    });
                    break;
                default:
                    filters.push({
                        "filterItemType": "single",
                        "filterOn": filterItem.key,
                        "filterType": filterItem.type,
                        "filterValues": filterItem.value.split(',')    //要考虑in的情况，界面是文本框，服务器需要的是数组，所以split
                    });
                    break;
            }
        }

        if (filters.length > 1) {
            queryEntity.filter = {
                "children": filters,
                "concatType": "and",
                "filterItemType": "group"
            }
        }
        else if (filters.length > 0) {
            queryEntity.filter = filters[0];
        }


        if (this.config.selectOrderBy && this.config.selectOrderBy.length > 0) {
            queryEntity.orderBys = [];
            for (var i = 0; i < this.config.selectOrderBy.length; i++) {
                var orderBy = this.config.selectOrderBy[i];
                if (isNaN(Number(orderBy.direction))) {
                    //默认按照升序
                    orderBy.direction = 0;
                }
                queryEntity.orderBys.push({
                    orderColumn: orderBy.key,
                    orderDirection: orderBy.direction
                });
            }
        } else {
            delete queryEntity.orderBys;
        }


        if (this.config.selectTopCount > 0) {
            queryEntity.paging = {
                pageIndex: 0,
                pageSize: this.config.selectTopCount
            }
        } else {
            delete queryEntity.paging;
        }


        //如果当前widget含有特殊报表选项，则增加相关选项
        if (this.config.specialApiTypeOption) {
            for (var key in this.config.specialApiTypeOption) {
                queryEntity.options.optionPairs.push({
                    key: key,
                    value: this.config.specialApiTypeOption[key]
                });
            }
        }


        return param;
    }

    return WidgetExtend;
})