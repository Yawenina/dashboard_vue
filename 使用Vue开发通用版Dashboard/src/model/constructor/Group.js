﻿define(['model/constructor/BaseModel'], function (BaseModel) {

    var Group = function () {
        BaseModel.call(this);
        this.logoPath = '';
    }

    Group.prototype = new BaseModel();
    //因为使用了继承，在BaseModel类的方法中，this.constructor获取的并不是子类，而是基类——BaseModel，所以单独设置了contructor属性
    Group.prototype.constructor = Group;

    /**
     * 获取指定groupId的分组信息(id属性必须赋值)
     * @return Ajax请求的$promise
     */
    Group.prototype.get = function () {
        return this.$resource.get({ module: 'dashboardGroup', id: this.id }).then(function (response) {
            var groups = this.convertDataToList(response.data);
            this.prototypeCopy(groups[0]);
        }.bind(this));
    }

    /**
     * 获取全部分组数据
     * @return Ajax请求的$promise
     */
    Group.prototype.getAll = function () {
        return this.$resource.get({ module: 'dashboardGroup' });
    }

    /**
     * 保存分组数据
     * @param isCreate 是否是保存新创建的分组
     * @param logoInputElement logo文件表单元素
     * @return Ajax请求的$promise
     */
    Group.prototype.save = function (isCreate, logoInputElement) {

        var param = this.getOwnProperty(function (propName, propValue) {
            //排除框架自带的属性
            if (propName.indexOf('$') > -1) {
                return false;
            }
            return true;
        });

        if (isCreate) {
            //如果是创建的话，是没有id值的
            delete param.id;
        }

        //采用HTML5里的window.FormData对象，让ajax也能提交二进制文件
        var formData = new FormData();
        for (var key in param) {
            formData.append(key, param[key]);
        }
        //“logo”名称是固定的，因为服务器端的DashboardGroup对象使用的是Logo这个属性名
        formData.append('logo', logoInputElement.files[0]);

        if (isCreate) {
            return this.$resource.save({ module: 'dashboardGroup' }, formData).then(function (response) {
                var group = response.data.Result;
                this.id = group.Id;
                this.logoPath = group.LogoPath;
            }.bind(this));
        } else {
            //return this.$resource.update({ module: 'dashboardGroup' }, formData).then(function (response) {
            //    console.log(response);
            //});
            return this.$resource.save({ module: 'dashboardGroup', action: 'update' }, formData).then(function (response) {
                console.log(response);
            }.bind(this));
        }
    }

    /**
     * 删除分组数据
     * @return Ajax请求的$promise
     */
    Group.prototype.delete = function () {
        return this.$resource.delete({ module: 'dashboardGroup', id: this.id }).then(function (response) {
            console.log(response);
        });
    }

    return Group;
})