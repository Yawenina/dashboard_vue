﻿define(['model/constructor/BaseModel'], function (BaseModel) {

    var ShareUser = function (dashboardId, email, permissionId) {
        BaseModel.call(this);

        this.dashboardId = dashboardId;
        this.id = email;   //用email作为关键字
        //this.email = '';
        this.permissionId = permissionId;
        this.status = 'create';         //create，update，noModify，delete
    }

    ShareUser.prototype = new BaseModel();
    ShareUser.prototype.constructor = ShareUser;

    /**
     * 获取当前仪表盘下所有共享用户数据
     * @returns {jQuery.Deferred} Ajax返回的Deferred对象
     */
    ShareUser.prototype.getAll = function () {
        return this.$resource.get({ module: 'userDashboardShare', action: 'GetShareByDashboardId', id: this.dashboardId })
            .then(function (response) {
                if (response.data.Result) {
                    var result = response.data.Result;
                    if (result instanceof Array) {
                        //处理服务器端属性名与客户端不一致的问题以及初始状态
                        for (var i = 0; i < result.length; i++) {
                            //用email作为id，以便ShareService中能用id作为关键字直接操作
                            result[i].id = result[i].Email;

                            result[i].permissionId = result[i].ShareRoleId;
                            delete result[i].ShareRoleId;

                            result[i].status = 'noModify';
                        }
                    }
                }

                return response;
            });
    }

    /**
     * 保存共享用户数据
     * @param {Array<ShareUser>} shareUsers 一组共享用户
     * @returns {jQuery.Deferred} Ajax返回的Deferred对象
     */
    ShareUser.prototype.save = function (shareUsers) {

        var params = [];
        
        for (var i = 0; i < shareUsers.length; i++) {
            switch (shareUsers[i].status) {
                case 'create':
                    shareUsers[i].shareType = 1;
                    break;
                case 'delete':
                    shareUsers[i].shareType = 2;
                    break;
                case 'update':
                    shareUsers[i].shareType = 3;
                    break;
                default:    //如果是未修改，则不作处理
                    continue;
            }

            //处理服务器端属性名与客户端不一致的问题
            shareUsers[i].shareRoleId = shareUsers[i].permissionId;
            //因为用email作为ShareService的id，所以提交服务器的时候要给email属性赋值
            shareUsers[i].email = shareUsers[i].id;

            var param = shareUsers[i].getOwnProperty(function (propName, propValue) {
                //排除框架自带的属性
                if (propName.indexOf('$') > -1) {
                    return false;
                }

                //只要以下几个属性
                switch (propName) {
                    case 'dashboardId':
                    case 'email':
                    case 'shareRoleId':
                    case 'shareType':
                        return true;
                }

                return false;
            });

            params.push(param);
        }

        if (params.length > 0) {
            return this.$resource.save({ module: 'UserDashboardShare', action: 'share' }, JSON.stringify(params));
        }

        return $.Deferred().resolve();
    }

    /**
     * 获取所有共享权限
     * @returns {jQuery.Deferred} Ajax返回的Deferred对象
     */
    ShareUser.prototype.getAllPermission = function () {
        return this.$resource.get({ module: 'ShareRole' });
    }

    return ShareUser;
})