﻿define(['model/constructor/BaseModel'], function(BaseModel) {
    
    var UserInfo = function() {
        BaseModel.call(this);
        this.token = '';
        this.email = '';
    }

    UserInfo.prototype = new BaseModel();
    //因为使用了继承，在BaseModel类的方法中，this.constructor获取的并不是子类，而是基类——BaseModel，所以单独设置了contructor属性
    UserInfo.prototype.constructor = UserInfo;

    /**
     * 获取全部用户
     * @returns {jQuery.Deferred} Ajax请求返回并处理后的Deferred对象
     */
    UserInfo.prototype.getAll = function() {
        return this.$resource.get({ module: 'user' });
    }

    /**
     * 保存用户与角色的映射关系
     * @param users 用户id与角色id对应关系，可以是多个
     * @returns {jQuery.Deferred} Ajax请求返回并处理后的Deferred对象
     */
    UserInfo.prototype.saveRole = function (users) {
       return this.$resource.save({ module: 'role', action: 'GrantPrivilege' }, JSON.stringify(users));
    }

    return UserInfo;
})