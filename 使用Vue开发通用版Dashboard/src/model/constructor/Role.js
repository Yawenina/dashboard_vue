﻿define(['model/constructor/BaseModel'], function (BaseModel) {

    var Role = function () {
        BaseModel.call(this);
        this.privilege = 0;
    }

    Role.prototype = new BaseModel();
    //因为使用了继承，在BaseModel类的方法中，this.constructor获取的并不是子类，而是基类——BaseModel，所以单独设置了contructor属性
    Role.prototype.constructor = Role;

    /**
     * 获取全部用户角色
     * @returns {jQuery.Deferred} Ajax请求返回并处理后的Deferred对象
     */
    Role.prototype.getAll = function () {
        return this.$resource.get({ module: 'role' });
    }

    /**
     * 保存用户角色
     * @param isCreate 是否是新建角色
     * @param roles 新增或者修改的角色列表
     * @returns {jQuery.Deferred} Ajax请求返回并处理后的Deferred对象
     */
    Role.prototype.save = function (isCreate, roles) {
        if (isCreate) {
            return this.$resource.save({ module: 'role' }, JSON.stringify(roles));
        }
        return this.$resource.save({ module: 'role', action: 'update' }, JSON.stringify(roles));
    }

    /**
     * 删除用户角色
     * @param roles 要删除的角色id列表     
     * @returns {jQuery.Deferred} Ajax请求返回并处理后的Deferred对象
     */
    Role.prototype.delete = function(roles) {
        return this.$resource.save({ module: 'role', action: 'deleteBunch' }, JSON.stringify(roles));
    }

    return Role;
})