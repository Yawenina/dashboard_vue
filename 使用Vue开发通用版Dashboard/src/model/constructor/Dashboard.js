﻿define(['model/constructor/BaseModel'], function (BaseModel) {

    var Dashboard = function (groupId) {
        BaseModel.call(this);
        this.groupId = groupId;
        this.config = {
            isDisableChangeGroup: false,    //是否不能改变所属分组
            templateUrl: 'dashboardLayoutTemplate.html',    //因为都是一样的表格布局，所以所有Dashboard的模板都是一样的
            filterLayoutConfig: {
                rows: 0,
                cols: 0,
                vMargin: 5,
                hMargin: 5
            },
            uiLayoutConfig: {
                rows: 12,
                cols: 12,
                vMargin: 5,
                hMargin: 5
            },
            icon: '',
            control: {}
        }
    }

    Dashboard.prototype = new BaseModel();
    //因为使用了继承，在BaseModel类的方法中，this.constructor获取的并不是子类，而是基类——BaseModel，所以单独设置了contructor属性
    Dashboard.prototype.constructor = Dashboard;

    /**
     * 获取指定dashboardId的仪表盘信息(id属性必须赋值)
     * @return Ajax请求的$promise
     */
    Dashboard.prototype.get = function () {
        return this.$resource.get({ module: 'dashboard', id: this.id }).then(function (response) {
            var dashboards = this.convertDataToList(response.data);
            this.prototypeCopy(dashboards[0]);
        }.bind(this));
    }

    /**
     * 获取指定分组下的全部仪表盘数据
     * @return Ajax请求的$promise
     */
    Dashboard.prototype.getAll = function () {
        return this.$resource.get({ module: 'dashboard', action: 'getByGroup', id: this.groupId });
    }

    /**
     * 保存仪表盘数据
     * @param isCreate 是否是新建
     * @return Ajax请求的$promise
     */
    Dashboard.prototype.save = function (isCreate) {

        var param = this.getOwnProperty(function (propName, propValue) {
            //排除框架自带的属性
            if (propName.indexOf('$') > -1) {
                return false;
            }
            return true;
        });

        if (isCreate) {
            //如果是创建的话，是没有id值的
            delete param.id;

            return this.$resource.save({ module: 'dashboard' }, JSON.stringify([param])).then(function (response) {
                this.id = response.data.Result[0];
            }.bind(this));
        } else {
            //return this.$resource.update({ module: 'dashboard' }, JSON.stringify([param])).then(function (response) {
            //    console.log(response);
            //});
            return this.$resource.save({ module: 'dashboard', action: 'update' }, JSON.stringify([param])).then(function (response) {
                console.log(response);
            });
        }
    }

    /**
     * 删除仪表盘数据
     * @return Ajax请求的$promise
     */
    Dashboard.prototype.delete = function () {
        //return this.$resource.delete({ module: 'dashboard', id: this.id }).then(function (response) {
        //    console.log(response);
        //});
        return this.$resource.get({ module: 'dashboard', action:'delete', id: this.id }).then(function (response) {
            console.log(response);
        });
    }

    return Dashboard;
})