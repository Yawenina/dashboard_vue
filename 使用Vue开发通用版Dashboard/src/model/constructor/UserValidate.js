﻿define(['model/constructor/BaseModel'], function (BaseModel) {

    var addressBarParam = {};
    (function () {
        var addressBarContent = window.location.search || window.location.hash;
        if (addressBarContent.indexOf('?') > -1 || addressBarContent.indexOf('#') > -1) {
            addressBarContent = addressBarContent.substr(1);
        }
        addressBarContent.split('&').forEach(function (param) {
            var keyValuePair = param.split('=');
            if (keyValuePair.length === 2) {
                addressBarParam[keyValuePair[0]] = keyValuePair[1];
            } else {
                addressBarParam[keyValuePair] = keyValuePair;
            }
        });
    })();

    var UserValidate = function () {
        BaseModel.call(this);
    }

    UserValidate.prototype = new BaseModel();
    //因为使用了继承，在BaseModel类的方法中，this.constructor获取的并不是子类，而是基类——BaseModel，所以单独设置了contructor属性
    UserValidate.prototype.constructor = UserValidate;

    /**
     * 获取指定key的queryString
     * @param key 指定的QueryStringKey
     * @returns 指定的Key对应的value
     */
    UserValidate.prototype.getQueryString = function (key) {
        return addressBarParam[key];
    }

    /**
     * 启动用户验证
     * @return Ajax请求的$promise
     */
    UserValidate.prototype.start = function (fullTicketId) {
        if (typeof fullTicketId !== 'string' || fullTicketId.length < 1) {
            fullTicketId = this.getQueryString('FullTicketId');
        }

        if (!fullTicketId) {
            this.jumpToLogin();
            return null;
        }

        //取消地址栏的FullTicketId
        var json = { time: new Date().getTime() };
        window.history.pushState(json, "", "/");

        //return this.$resource.get({ module: 'security', action: 'validate', id: fullTicketId });
        return this.$resource.save({ module: 'security', action: 'validate' }, { ticketId: fullTicketId });
    }

    /**
     * 跳转到登录页面
     */
    UserValidate.prototype.jumpToLogin = function () {
        var url = window.location.href;
        var index = url.lastIndexOf('?');
        if (index > -1) {
            url = url.substr(0, index);
        }
        window.location.href = '//www.gridsumdissector.com/zh-cn/sso.aspx?returnurl=' + encodeURIComponent(url) + '&method=sso&product=271f4236-3b8a-4640-b978-9317c6efa194';
    }

    /**
     * 登出系统
     * @param token 用户token
     */
    UserValidate.prototype.logout = function (token) {
        this.$resource.get({ module: 'security', action: 'logout' }, { token: encodeURIComponent(token) })
               .then(function (response) {
                   if (response && response.data.Result === true) {
                       this.jumpToLogin();
                   }
               }.bind(this));
    }

    return UserValidate;
})