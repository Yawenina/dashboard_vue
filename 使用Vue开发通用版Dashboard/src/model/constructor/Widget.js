define(["model/constructor/BaseModel"], function (BaseModel) {

    var Widget = function (dashboardId) {
        BaseModel.call(this);

        //小部件使用的已实例化的图表对象
        this.chartObj = null;
        //服务器端需要的属性
        this.dashboardId = dashboardId;
        this.productId = -1;
        this.solutionId = -1;
        this.profileId = -1;
        this.apiType = -1;      //apiType的值对应后台服务配置文件里的key，这样可以在对接其他产品时灵活配置
        /*
         * @todo 目前有个想法，给Widget增加widgetType字段，里面三个属性：large、small、number。
         * @todo 再用Object.defineProperty定义chartType，在set方法里根据chartType的值修改widgetType里的属性值
         */
        this.chartType = -Infinity;
        this.displayCount = 0;        
        this.config = {
            status: 'noModify', //create，update，noModify
            layout: {
                x: 0,
                y: 0,
                row: 0,
                col: 0
            },
            specialApiTypeOption: {},   //图表配置里选择的特殊报表选项（如：AD的频次报表、SEMD的普通报表）
            selectDimension: [],        //图表配置里选择的维度
            selectIndicator: [],        //图表配置里选择的指标
            selectFilterCondition: [],  //图表配置里选择的过滤
            selectOrderBy: [],          //图表配置里选择的排序
            selectTopCount: 0,          //图表配置里选择的数量
            widgetName: '',             //widget名称，可以从availableWidgetService中根据name获取availableWidget
            chartOption: {},            //保存用户调整的不包含数据的图表选项            

            dataSource: [],
            filterContent: {},
            chartShowClass: ''
        };
    }

    Widget.prototype = new BaseModel;
    Widget.prototype.constructor = Widget;        

    /**
     * 获取指定dashboardId的小部件信息(id属性必须赋值)
     * @return Ajax请求的$promise
     */
    Widget.prototype.get = function () {
        return this.$resource.get({ module: 'chart', id: this.id }).then(function (response) {
            var dashboards = this.convertDataToList(response.data);
            this.prototypeCopy(dashboards[0]);
        }.bind(this));
    }

    /**
     * 获取指定分组下的全部小部件数据
     * @return Ajax请求的$promise
     */
    Widget.prototype.getAll = function () {
        return this.$resource.get({ module: 'chart', action: 'getByDashboard', id: this.dashboardId }).then(function (response) {
            var datas = response.data.Result;
            for (var i = 0; i < datas.length; i++) {
                //处理服务器端属性名与客户端不一致的问题
                datas[i].config = datas[i].ChartConfig;
                delete datas[i].ChartConfig;
            }
            return response;
        });
    }

    /**
     * 保存小部件数据
     * @param isCreate 是否是新建
     * @return Ajax请求的$promise
     */
    Widget.prototype.save = function (isCreate) {

        var param = this.getOwnProperty(function (propName) {
            //排除服务器不需要的属性
            if (propName.indexOf('$') > -1 ||
                propName.indexOf('chartObj') > -1) {
                return false;
            }
            return true;
        });

        //处理服务器端属性名与客户端不一致的问题
        param.chartConfig = param.config;
        delete param.config;

        if (isCreate) {
            //如果是创建的话，是没有id值的
            delete param.id;

            return this.$resource.save({ module: 'chart' }, JSON.stringify([param])).then(function (response) {
                this.id = response.data.Result[0];
            }.bind(this));
        } else {
            //return this.$resource.update({ module: 'chart' }, JSON.stringify([param])).then(function (response) {
            //    console.log(response);
            //});
            return this.$resource.save({ module: 'chart', action: 'update' }, JSON.stringify([param])).then(function (response) {
                console.log(response);
            });
        }
    }

    /**
     * 删除小部件数据
     * @return Ajax请求的$promise
     */
    Widget.prototype.delete = function () {
        //return this.$resource.delete({ module: 'chart', id: this.id }).then(function (response) {
        //    console.log(response);
        //});
        return this.$resource.get({ module: 'chart', action: 'delete', id: this.id }).then(function (response) {
            console.log(response);
        });
    }


    return Widget;
});