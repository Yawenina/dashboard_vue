﻿define(['service/resource'], function ($resource) {

    var BaseModel = function () {
        this.id = -Infinity;
        this.name = '';
        this.config = {};
        this.$resource = $resource;
    }

    /**
     * 将指定对象的属性复制到当前对象上
     * @param obj 要复制的对象
     * @param fnIsCopy(propName, propValue):boolean 是否复制属性
     */
    BaseModel.prototype.prototypeCopy = function (obj, fnIsCopy, isNoNeedConvertPropName) {
        for (var propName in obj) {
            if (obj.hasOwnProperty(propName) && typeof obj[propName] !== 'function') {
                var isCopy = true;
                if (typeof fnIsCopy === 'function') {
                    isCopy = fnIsCopy(propName, obj[propName]);
                }
                if (isCopy) {
                    /**
                     * 需求变更：
                     * 在对象clone当中，从服务器端返回的数据属性名第一个字母都是大写，需要将属性名的首字母变成小写
                     * 但也有特殊情况，比如specialApiTypeOption，因为请求时OptionPairs里附加的key的值大小写是区分的
                     * 所以对specialApiTypeOption里的子属性名不做小写转换
                     * 
                     * 解决方案：
                     * 1、增加一个参数，判断是否不需要进行小写转换
                     * 2、判断当前propName是否是specialApiTypeOption，是的话，在递归调用时，给是否不需要小写转换参数设置成true
                     * 
                     * 申请人员：赵泽彬、张昭
                     * 申请日期：2016-03-17
                     */
                    var newPropName = propName;
                    if (!isNoNeedConvertPropName) {
                        //因为从服务器端返回的数据属性名第一个字母都是大写，需要将属性名的首字母变成小写
                        newPropName = propName.replace(/\w/, function (char) {
                            return char.toLowerCase();
                        });                        
                    }

                    //是否对当前对象的子属性名称进行首字母小写转换
                    var isNoNeedConvertChildPropName = false;
                    switch (newPropName) {
                        case 'specialApiTypeOption':
                            isNoNeedConvertChildPropName = true;
                            break;
                    }

                    if (typeof obj[propName] === "object") {
                        //排除null
                        if (!obj[propName]) {
                            continue;
                        }
                        else if (obj[propName] instanceof Array) {
                            this[newPropName] = [];
                            obj[propName].forEach(function (value) {
                                var _this = this.context,
                                    prop = _this[this.propName];
                                if (typeof value === "object") {
                                    //为了深度递归时，this有prototypeCopy方法而准备的
                                    prop = {
                                        prototypeCopy: _this.prototypeCopy
                                    }
                                    prop.prototypeCopy(value, fnIsCopy, this.isNoNeedConvertChildPropName);
                                    delete prop.prototypeCopy;

                                    _this[this.propName].push(prop);
                                } else {
                                    prop.push(value);
                                }
                            }.bind({
                                context: this,
                                propName: newPropName,
                                isNoNeedConvertChildPropName: isNoNeedConvertChildPropName
                            }));
                        } else {
                            //为了深度递归时，this有prototypeCopy方法而准备的
                            this[newPropName] = {
                                prototypeCopy: this.prototypeCopy
                            }
                            this[newPropName].prototypeCopy(obj[propName], fnIsCopy, isNoNeedConvertChildPropName);
                            delete this[newPropName].prototypeCopy;
                        }
                    } else {
                        this[newPropName] = obj[propName];
                    }
                }
            }
        }
    }

    /**
     * 将服务器端返回的数据转换成集合对象
     * @param response  返回的结果集
     * @returns {Array<Model.Constructor>} 转换完成的集合对象
     */
    BaseModel.prototype.convertDataToList = function (response) {
        var objects = [];
        response.Result.forEach(function (oneData) {
            var obj = new this.constructor();
            obj.prototypeCopy(oneData);
            objects.push(obj);
        }.bind(this));
        return objects;
    }

    /**
     * 获取当前对象的所有非方法属性
     * @param fnIsCopy(propName, propValue):boolean 是否复制属性
     * @returns 返回具有所有非方法属性的对象
     */
    BaseModel.prototype.getOwnProperty = function (fnIsCopy) {
        //因为在prototypeCopy方法里使用了深度递归，在prototypeCopy方法中的this必须具有prototypeCopy方法
        //所以在这里给obj赋予prototypeCopy方法，并调用，而不是直接采用call的方式调用
        var obj = {
            prototypeCopy: this.prototypeCopy
        };
        obj.prototypeCopy(this, fnIsCopy);
        delete obj.prototypeCopy;
        return obj;
    }

    /**
     * 将返回的数据转换成对象，并添加到指定Service上
     * 如果需要对转换后的对象附加额外的属性，需要调用convertDataToList方法后自己循环添加
     * 因为Vuejs会在数组的push方法调用时，对添加的对象所有属性增加get与set
     * 如果调用此方法后，再附加额外的属性，此属性发生的任何改变，是不会被Vuejs监测到的
     * @param response 返回的数据
     * @param servObj 指定的Service对象
     */
    BaseModel.prototype.addResponseDataToServObj = function (response, servObj) {
        servObj.delAll();
        this.convertDataToList(response).forEach(function (obj) {
            servObj.add(obj);
        });
    }

    return BaseModel;
})