/**
 * 定义全局model，保证APP中只有一组数据。所有View都会绑定这个model
 */
define([], function () {

    return {
        userInfo: null,
        group: null,
        dashboard: null,
        widget: null,
        availableWidget: null,
        datasource: null,
        users: null,
        share: null
    }
});