﻿define(['vue', 'text!template/appContent/dashboardShare.html',
        'service/userInfo', 'service/users', 'service/group', 'service/dashboard', 'service/share'
],
    function (Vue, tmpl, userInfoServ, usersServ, groupServ, dashboardServ, shareServ) {

        return Vue.component('dashboard-share', {
            template: tmpl,
            data: function () {
                return {
                    userInfoServ: userInfoServ,
                    usersServ: usersServ,
                    groupServ: groupServ,
                    dashboardServ: dashboardServ,
                    shareServ: shareServ,
                    selectObjectId: {
                        //保存当前选中的id
                        group: -Infinity,
                        dashboard: -Infinity
                    },
                    users: [],                      //合法用户列表
                    dashboards: [],
                    sharePermissions: [],
                    inputEmail: '',
                    isInputEmailHasError: false,    //是否输入的Email有错误
                    inputNewUserPermission: 10001,  //默认管理权限
                    isChangeDataStatus: false,      //数据是否发生变动
                    isSelectAllUser: false          //是否选择全部用户
                }
            },
            computed: {
                groups: function () {
                    return this.groupServ.getAll();
                },
                shareUsers: function () {
                    return this.shareServ.getAll();
                },
                isDisableShare: function () {
                    var dashboard = this.getSelectDashboard();
                    return dashboard && (dashboard.privilege & 4) !== 4;
                },
                isShowOperateButton: function () {
                    return this.dashboards.length > 0;
                }
            },
            methods: {
                getSelectDashboard: function () {
                    //因为此时选中的分组与顶部选择的分组不一致，也就是说，dashboardServ中存储的list与dashboards存储的不一致。
                    //所以不能用dashboardServ提供的getById方法
                    for (var i = 0; i < this.dashboards.length; i++) {
                        if (this.selectObjectId.dashboard === this.dashboards[i].id) {
                            return this.dashboards[i];
                        }
                    }
                    return null;
                },
                initAutoComplete: function () {
                    var $element = $(this.$el).find('input[type="email"]');
                    $element
                        .click(function () {

                            var $input = $(this);

                            //选择全部文本内容
                            $input.select();

                            //触发autoComplete的search，这样在点击后也能显示搜索下拉菜单
                            $input.autocomplete('search', $input.val());
                        })
                        .autocomplete({
                            appendTo: $element.parent(),
                            source: this.users,
                            minLength: 1, //设置输入几个字符后就可以搜索
                            response: function (event, ui) {

                                var creatorIndex = -1,
                                    showCount = 10;
                                //因为只显示showCount条记录，所以只循环showCount次。考虑到有可能在showCount中有创建者，所以+1
                                for (var i = 0; i < showCount + 1 && i < ui.content.length; i++) {
                                    //找到创建者所在的位置
                                    if (ui.content[i].id === this.getSelectDashboard().creatorId) {
                                        creatorIndex = i;
                                        continue;
                                    }
                                }

                                //排除创建者
                                if (creatorIndex > -1) {
                                    ui.content.splice(creatorIndex, 1);
                                }

                                //调整显示条数
                                if (ui.content.length > showCount) {
                                    //只保留showCount数量的记录
                                    ui.content.splice(showCount, ui.content.length - showCount);
                                }

                            }.bind(this),
                            select: function (event, ui) {
                                //添加的用户必须从列表中选择，而不是通过v-model绑定input，防止添加不合法用户
                                this.inputEmail = ui.item.label;

                                /**
                                 * 需求变更：选中一个用户以后，直接增加
                                 * 申请人员：赵泽彬
                                 * 申请日期：2016-03-07
                                 */
                                this.addUser();
                            }.bind(this),
                            close: function (event, ui) {
                                /**
                                 * 需求变更：选中一个用户以后，直接增加，增加后，清除文本框里的内容
                                 * 申请人员：赵泽彬
                                 * 申请日期：2016-03-07
                                 */
                                event.target.value = '';
                            }
                        });
                },
                onChange: function (item, id, isEnforceExecute) {
                    if (this.selectObjectId[item] === id && isEnforceExecute !== true) {
                        return;
                    }

                    if (this.isChangeDataStatus) {
                        if (!confirm('是否放弃对当前仪表盘共享用户的调整？')) {
                            return;
                        }
                        this.isChangeDataStatus = false;
                    }

                    this.selectObjectId[item] = id;
                    switch (item) {
                        case 'group':
                            //改变分组时，上一个分组所选择的仪表盘ID也就意味着无效了
                            this.selectObjectId.dashboard = -Infinity;

                            //根据分组id获取仪表盘列表
                            this.dashboardServ.sendRequest(id, function (response, dashboardObj) {
                                this.dashboards = dashboardObj.convertDataToList(response);

                                if (this.dashboards.length > 0) {
                                    //默认是第一个仪表盘
                                    var index = 0;
                                    //如果选择的分组是当前分组，则用当前选中的仪表盘作为分享的默认仪表盘
                                    if (this.selectObjectId.group === this.groupServ.get().id) {
                                        index = this.dashboardServ.getIndex();
                                    }
                                    //设置默认仪表盘选中状态
                                    this.onChange('dashboard', this.dashboards[index].id);
                                } else {
                                    //如果没有仪表盘，不会调用onChange('dashboard')方法，没法自动清除上一个仪表盘的共享用户数据。
                                    this.shareServ.delAll();
                                }
                            }.bind(this));
                            break;
                        case 'dashboard':
                            //根据仪表盘id获取共享列表
                            this.shareServ.requestAll(id).then(function (shareUsers) {
                                //增加创建者
                                var creator = this.shareServ.createNew(this.selectObjectId.dashboard, this.getSelectDashboard().creatorEmail, 0);
                                //因为不能将仪表盘分享给这个仪表盘的创建者，所以将create状态改为noModify，这样保存时就不会提交
                                creator.status = 'noModify';
                                shareUsers.unshift(creator);
                            }.bind(this));
                            break;

                    }
                },
                isCurrent: function (item, id) {
                    return this.selectObjectId[item] === id;
                },
                selectAllUser: function ($event) {
                    for (var i = 0; i < this.shareUsers.length; i++) {
                        var shareUser = this.shareUsers[i];
                        if (this.isCreator(shareUser)) {
                            continue;
                        }
                        shareUser.isSelected = $event.target.checked;
                    }
                },
                selectOneUser: function (shareUser) {
                    if (!shareUser.isSelected) {
                        this.isSelectAllUser = false;
                    }
                },
                addUser: function () {
                    if (this.inputEmail.length === 0) {
                        return;
                    }

                    //排除已添加或已存在的用户
                    for (var i = 0; i < this.shareUsers.length; i++) {
                        if (this.shareUsers[i].id === this.inputEmail) {
                            this.isInputEmailHasError = true;
                            return;
                        }
                    }

                    //创建一个新的用户共享对象，new ShareUser时，status会被设置为create
                    var shareUser = this.shareServ.createNew(this.selectObjectId.dashboard, this.inputEmail, this.inputNewUserPermission);
                    this.shareServ.add(shareUser);

                    //清空输入框
                    $(this.$el).find('input[type="email"]').val('');

                    //改变当前仪表盘用户分享数据状态
                    this.isChangeDataStatus = true;
                },
                delUser: function (id) {
                    this.shareServ.getById(id).status = 'delete';
                    this.shareServ.delUser(id);

                    //改变当前仪表盘用户分享数据状态
                    this.isChangeDataStatus = true;
                },
                delSelectedUser: function () {
                    /**
                     * 不能在for循环中直接调用this.delUser，因为会影响到循环的次数
                     */
                    var delUserId = [];
                    for (var i = 0; i < this.shareUsers.length; i++) {
                        if (!this.shareUsers[i].isSelected) {
                            continue;
                        }
                        delUserId.push(this.shareUsers[i].id);
                    }

                    for (var j = 0; j < delUserId.length; j++) {
                        this.delUser(delUserId[j]);
                    }
                },
                changeUserPermission: function (shareUser) {
                    shareUser.status = 'update';

                    //改变当前仪表盘用户分享数据状态
                    this.isChangeDataStatus = true;
                },
                save: function () {
                    this.shareServ.save().then(function (response) {
                        if (!response) {
                            alert('共享用户没有发生变动，不需要保存');
                            return;
                        }
                        if (response instanceof Array) {
                            if (response.length === 0) {
                                alert('保存共享用户成功');
                                return;
                            }

                            //@todo 标识出出错的用户。目前是通过选择用户来实现的，暂时不需要此操作

                            return;
                        }

                        //处理Status不是200的情况
                        console.debug(response);
                    });
                },
                cancel: function () {
                    //强制从服务器重新获取数据，并刷新展示界面
                    this.onChange('dashboard', this.selectObjectId.dashboard, true);
                },
                isCreator: function (shareUser) {
                    return shareUser.permissionId === 0;
                }
            },
            created: function () {
                //获取共享权限
                this.shareServ.requestAllPermission().then(function (permissions) {
                    this.sharePermissions = permissions;
                }.bind(this));

                //获取用户列表
                this.usersServ.requestAll().then(function (users) {
                    this.users = [];
                    for (var i = 0; i < users.length; i++) {
                        //排除当前用户
                        if (users[i].id === this.userInfoServ.id) {
                            continue;
                        }

                        this.users.push({
                            id: users[i].id,
                            label: users[i].email
                        });
                    }

                    //初始化输入提示框
                    this.initAutoComplete();
                }.bind(this));
            },
            ready: function () {
                //默认展示当前分组下当前仪表盘的共享用户数据
                this.onChange('group', this.groupServ.get().id);


            }
        });

    })