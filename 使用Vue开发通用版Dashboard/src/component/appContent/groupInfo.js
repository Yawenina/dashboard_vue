﻿define(['vue', 'text!template/appContent/groupInfo.html', 'service/group'],
    function (Vue, tmpl, groupServ) {

        return Vue.component('group-info', {
            template: tmpl,
            props: {
                tempGroup: {
                    type: Object,
                    required: true
                }
            },
            data: function () {
                return {
                    groupServ: groupServ,
                    warnMessage: ''
                }
            },
            computed: {
                title: function () {
                    return '新建仪表盘分组';
                }
            },
            methods:{
                'saveGroup': function () {

                    //判断新建或者保存的分组名称是否与已存在的分组有重复
                    var groups = this.groupServ.getAll();
                    for (var i = 0; i < groups.length; i++) {
                        var group = groups[i];
                        //因为View的input是绑定到了tempDashboard.name，如果不加上不是同一个widgetId判断，这个判断永远为true
                        if (group.id !== this.tempGroup.id && group.name === this.tempGroup.name) {
                            this.warnMessage = '分组名称已存在';
                            return;
                        }
                    }

                    var isCreate = true;
                    if (this.tempGroup.id > 0) {
                        isCreate = false;
                    }
                    this.tempGroup.save(isCreate, document.getElementById('groupLogoPath-file')).then(function () {
                        var index = this.groupServ.getIndexById(this.tempGroup.id);
                        //如果存在，则删除已有的，再将临时的添加进来。如果不存在，则直接添加
                        if (index > -1) {
                            this.groupServ.del(index);
                        }
                        //将临时的分组对象添加到groupService中
                        this.groupServ.add(this.tempGroup);

                        //关闭当前组件
                        this.cancelGroup();

                        //获取指定分组下的全部仪表盘
                        this.$dispatch('callSiblingEvent', 'dashboardList.groupChange', this.tempGroup.id);
                    }.bind(this));
                },
                'cancelGroup': function () {
                    this.$dispatch('appContent.loadModalComponent');
                }
            }
        });

    })