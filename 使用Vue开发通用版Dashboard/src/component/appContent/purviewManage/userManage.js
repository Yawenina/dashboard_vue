﻿define(['vue', 'text!template/appContent/purviewManage/userManage.html',
        'service/userInfo', 'service/users', 'component/paging/paging'
    ],
    function (Vue, tmpl, userInfoServ, usersServ, pagingComp) {

        return Vue.component('user-manage', {
            template: tmpl,
            components: {
                paging: pagingComp
            },
            data: function () {
                return {
                    userInfoServ: userInfoServ,
                    usersServ: usersServ,
                    allUsers: [],

                    inputSearchKey: '',
                    isSelectCurrentPageAllUser: false,
                    users: [],
                    roles: {},       //使用对象而不是数组，是因为需要根据id查找name
                    currentPageDatas: []
                }
            },
            methods: {
                searchUser: function () {
                    var searchKey = this.inputSearchKey.replace(/\s/g, '');
                    if (searchKey.length > 0) {
                        var searchResult = [];
                        for (var i = 0; i < this.allUsers.length; i++) {
                            var user = this.allUsers[i];
                            if (user.email.indexOf(searchKey) > -1) {
                                searchResult.push(user);
                                continue;
                            }
                        }
                        this.users = searchResult;
                    } else {
                        this.users = this.allUsers;
                    }
                    
                    this.$nextTick(function () {
                        //默认打开第一页
                        this.$broadcast('paging.turnToPage', 0, true);
                    });
                },
                save: function () {
                    var users = [],
                        params = [];
                    for (var i = 0; i < this.users.length; i++) {
                        var user = this.users[i];
                        if (!user.isSelected) {
                            continue;
                        }
                        params.push({
                            userId: user.id,
                            roleId: user.role.id
                        });
                        users.push(user);
                    }
                    if (params.length === 0) {
                        return;
                    }
                    this.usersServ.saveUserAndRoleMap(params).then(function (response) {
                        if (response.Status === 200) {
                            alert('修改用户的角色保存成功！');

                            var _this = this.context,
                                users = this.users;

                            for (var i = 0; i < users.length; i++) {
                                var user = users[i];
                                //修改界面上显示的名称
                                user.grantorName = _this.userInfoServ.email;
                                user.grantTime = _this.formatToDateString();
                                user.role.name = _this.roles[user.role.id];
                                user.isSelected = false;
                            }

                        } else {
                            console.debug(response.Result);
                        }
                    }.bind({
                        context: this,
                        users: users
                    }));
                },
                formatToDateString: function (dateString) {
                    return require('moment')(dateString).format('YYYY-MM-DD');
                }
            },
            ready: function () {
                this.usersServ.requestAll().then(function (users) {
                    this.allUsers = this.users = users;
                    /**
                     * 如果给users赋值完，立即通过事件方式切换第一页，界面是空白的
                     * 因为在paging组件中effectDatas是空数组，Vuejs并不是立即更新
                     * 所以需要将执行方法延迟到下次 DOM 更新循环之后执行。
                     */
                    this.$nextTick(function () {
                        //默认打开第一页
                        this.$broadcast('paging.turnToPage', 0);
                    });
                }.bind(this));

                this.usersServ.requestAllRole().then(function (roles) {
                    for (var i = 0; i < roles.length; i++) {
                        var role = roles[i];
                        this.roles[role.id] = role.name;
                    }
                }.bind(this));
            }
        });

    })