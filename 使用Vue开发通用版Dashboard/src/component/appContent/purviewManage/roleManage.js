﻿define(['vue', 'text!template/appContent/purviewManage/roleManage.html',
        'service/role'
],
    function (Vue, tmpl, roleServ) {

        return Vue.component('role-manage', {
            template: tmpl,
            data: function () {
                return {
                    roleServ: roleServ,

                    roles: [],
                    permissions: [],    //这个permission对应的是每个角色的permission
                    allPermissions: [
                        {
                            id: Math.pow(2, 0),
                            name: '查看'
                        },
                        {
                            id: Math.pow(2, 5),
                            name: '导出'
                        },
                        {
                            id: Math.pow(2, 3) | Math.pow(2, 4) | Math.pow(2, 1),
                            name: '编辑仪表盘/组'
                        },
                        {
                            id: Math.pow(2, 2),
                            name: '共享仪表盘'
                        },
                        {
                            id: Math.pow(2, 20),
                            name: '权限管理'
                        }
                    ]
                }
            },
            computed: {
                allPermissionToPrivilege: function () {
                    var privilege = 0;
                    for (var i = 0; i < this.allPermissions.length; i++) {
                        privilege |= this.allPermissions[i].id;
                    }
                    return privilege;
                }
            },
            methods: {
                calculatePermission: function (privilege) {
                    var permissions = [];
                    for (var i = 0; i < this.allPermissions.length; i++) {
                        var permission = {};
                        //利用jQuery的extend方法，将permission对象克隆一份
                        $.extend(true, permission, this.allPermissions[i]);

                        var num = permission.id;
                        if ((privilege & num) === num) {
                            permission.isSelected = true;
                        } else {
                            permission.isSelected = false;
                        }
                        permissions.push(permission);
                    }

                    return permissions;
                },

                addRole: function () {
                    var role = this.roleServ.createNew('新角色');
                    //默认无任何权限 @雅雅 2016/3/10
                    role.privilege = 0;
                    role.permissions = this.calculatePermission(role.privilege);

                    //切换current
                    this.roleServ.get().isCurrent = false;
                    this.roleServ.add(role);
                    this.$nextTick(function() {
                        this.context.changeRoleStatus(this.role);
                    }.bind({
                        context: this,
                        role: role
                    }));

                },
                delRole: function () {
                    if (!confirm('是否删除当前角色？')) {
                        return;
                    }

                    this.roleServ.get().status = 'delete';

                    this.roleServ.delete().then(function (response) {
                        if (response instanceof Array && response.length > 0) {
                            alert('删除用户角色成功');
                            this.selectOneRole(this.roleServ.get());
                        }
                        else if (!response) {
                            alert('没有要删除的角色');
                        } else {
                            console.debug(response);
                            alert('删除用户角色失败');
                        }
                    }.bind(this));
                },
                inputRoleName_OnKeyUp: function (role) {
                    role.isSelected = false;
                },
                selectOneRole: function (role) {
                    //切换current
                    this.roleServ.get().isCurrent = false;

                    this.roleServ.setById(role.id);
                    //切换current
                    role.isCurrent = true;
                    this.permissions = role.permissions;
                },
                changeRoleStatus: function (role, status) {
                    if (!status) {
                        //通过View绑定isSelected属性，调整input的高度，将input显示出来
                        role.isSelected = true;
                        //让input获取focus，要不然点击其他地方无法触发失去焦点blur事件
                        this.$nextTick(function() {
                            document.getElementById('role_' + this.role.id).focus();
                        }.bind({
                            role: role
                        }))
                        
                    } else {
                        role.isSelected = false;
                        if (role.status === 'noModify') {
                            role.status = 'update';
                        }

                    }
                },
                save: function () {
                    //对处在非修改状态的每个角色验证权限是否发生变动
                    for (var i = 0; i < this.roles.length; i++) {
                        var role = this.roles[i];

                        //对于特殊用户不调整权限
                        if (role.status !== 'noModify' || role.privilege === -1) {
                            continue;
                        }

                        //计算选择的权限，判断是否与原有的权限一致
                        var privilege = 0;
                        for (var j = 0; j < role.permissions.length; j++) {
                            var permission = role.permissions[j];
                            if (permission.isSelected) {
                                privilege |= permission.id;
                            }
                        }

                        if (privilege !== role.privilege) {
                            role.privilege = privilege;
                            role.status = 'update';
                        }
                    }
                    this.roleServ.save().then(function (response) {
                        if (!response) {
                            alert('没有要修改的角色');
                        }
                        else if (response instanceof Array && response.length === 0) {
                            alert('更新用户角色成功');
                        }
                        else {
                            alert('更新用户角色失败');                            
                        }
                    });
                }
            },
            ready: function () {
                this.roleServ.requestAll().then(function (roles) {
                    for (var i = 0; i < roles.length; i++) {
                        var role = roles[i];
                        role.permissions = this.calculatePermission(role.privilege);
                    }

                    this.roles = roles;

                    /**
                     *  需求变更：角色管理默认选中第一个
                     *  申请人员：赵泽彬
                     *  申请日期：2016-3-11
                     */
                    if (roles.length > 0) {
                        this.selectOneRole(roles[0]);
                    }
                }.bind(this));
            }
        });

    })