﻿define(['vue', 'text!template/appContent/dashboardShow.html',
        'component/gridstack/gridstack', 'service/widget'],
    function (Vue, tmpl, gridstackComp, widgetServ) {

        return Vue.component('dashboard-show', {
            template: tmpl,
            components: {
                'grid-stack': gridstackComp
            },
            props: {
                passDashboard: {
                    type: Object,
                    required: true
                }
            },
            data: function () {
                return {
                    widgets: [],
                    widgetServ: widgetServ,
                    selectFilterItem: {}
                }
            },
            methods: {
                recordSelectFilterItem: function (filterItems) {
                    if (!filterItems) {
                        return;
                    }

                    if (!this.selectFilterItem) {
                        this.selectFilterItem = {};
                    }
                    //考虑到传递的filterItems有可能是arguments，因此不能用insteadof Array判断
                    if (typeof filterItems.length === 'undefined') {
                        filterItems = [filterItems];
                    }
                    for (var i = 0; i < filterItems.length; i++) {
                        var filterItem = filterItems[i];
                        //记录选择的过滤项
                        this.selectFilterItem[filterItem.key] = filterItem;
                    }
                }
            },
            events: {
                'dashboardShow.switchDashboard': function (dashboard) {

                    //如果此dashboard已存在，则通过事件传递过来的dashboard才是有效的
                    if (dashboard && this.passDashboard.id !== dashboard.id) {
                        this.passDashboard = dashboard;
                        //清除界面上已存在的widget，这样界面上就不会出现切换后仍然显示旧的仪表盘的widget
                        this.$broadcast('gridstack.removeAllItem');
                    }

                    //通知gridstack小部件，创建布局
                    this.$broadcast('gridstack.createLayout');

                    /**
                     * 只在此处获取仪表盘下的所有widget
                     * 因为每次切换仪表盘，appContent都先显示dashboard-show，所以widgetService.requestAll()放到了此处
                     */
                    //获取当前仪表盘下的所有小部件，并添加到界面中
                    this.widgetServ.requestAll(this.passDashboard.id).then(function (widgets) {

                        //获取全部过滤器的数据
                        var deferreds = [];
                        var filterWidgets = this.widgetServ.getByType('filter');
                        for (var i = 0; i < filterWidgets.length; i++) {
                            //@todo 因为现在只有一个日期过滤器，所以不需要发送请求，但以后要考虑怎么区分哪些过滤器需要发送请求
                            //deferreds.push(filterWidgets[i].getData());
                            
                            for (var j = 0; j < filterWidgets[i].config.selectFilterCondition.length; j++) {
                                var deferred = $.Deferred();
                                deferred.resolve(filterWidgets[i].config.selectFilterCondition[j]);
                                deferreds.push(deferred);
                            }
                        }

                        $.when.apply($, deferreds).then(function () {
                            //@todo 取每个过滤器的哪个过滤项？
                            this.context.recordSelectFilterItem(arguments);

                            //通过赋值，使用props的方式将所有widget传递到gridstack和gridstackItem上
                            this.context.widgets = this.widgets;
                        }.bind({
                            context: this,
                            widgets: widgets
                        }));
                      
                    }.bind(this));
                },
                /**
                 * 订阅过滤器变化事件，当过滤器发生变动的时候，会往上冒泡事件。
                 * 因为只有show需要过滤器与图表联动，所以只在dashboardShow中监听此事件
                 */
                'dashboardShow.filterChange': function (filterItems) {
                    this.recordSelectFilterItem(filterItems);
                    //往下通知图表刷新
                    this.$broadcast('chart.refreshChart', this.selectFilterItem);
                }
            },
            ready: function () {
                //this.widgetServ.delAll();
                this.$emit('dashboardShow.switchDashboard');
            }
        });

    })