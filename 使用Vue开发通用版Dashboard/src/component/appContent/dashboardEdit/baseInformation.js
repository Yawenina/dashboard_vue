﻿define(['vue', 'component/modal', 'text!template/appContent/dashboardEdit/baseInformation.html', 'service/group',
     'component/appContent/dashboardEdit/baseInformation/dashboardInfo'
    ],
    function (Vue, modalComp, baseInformationTmpl, groupServ, dashboardInfoComp) {

        return Vue.component('base-information', {
            template: baseInformationTmpl,
            props: {
                passDashboard: {
                    type: Object,
                    required: true
                }
            },
            data: function () {
                return {
                    loadModalComponentName: 'dashboard-info',   //默认展示编辑仪表盘信息界面，通过“新建分组”切换到分组信息界面                    
                }
            },
            components: {
                modal: modalComp,
                'dashboard-info': dashboardInfoComp
            }
        });


    })