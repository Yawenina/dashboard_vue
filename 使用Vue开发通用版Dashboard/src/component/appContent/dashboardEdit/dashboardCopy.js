﻿define(['vue', 'text!template/appContent/dashboardEdit/baseInformation/dashboardCopy.html',
        'service/group', 'service/dashboard'
],
    function (Vue, dashboardCopyTmpl, groupServ, dashboardServ) {

        return Vue.component('dashboard-info', {
            template: dashboardCopyTmpl,
            props: {
                tempDashboard: {
                    type: Object,
                    required: true
                }
            },
            data: function () {
                return {
                    groupServ: groupServ,
                    dashboardServ: dashboardServ,
                    groupName: '',
                    warnMessage: '',
                    dashboardCopyAction: ''         //仪表盘复制 or 移动
                }
            },
            computed: {
                title: function () {
                    return '复制/移动仪表盘';
                },
                groups: function () {
                    return this.groupServ.getAll();
                },
                config: function () {
                    return this.tempDashboard.config;
                }
            },
            methods: {
                groupList_OnChange: function (group) {
                    this.tempDashboard.groupId = group.id;
                    this.groupName = group.name;
                },
                save: function () {
                    var isCreate = false;
                    switch (this.dashboardCopyAction) {
                        case 'copy':
                            isCreate = true;
                            //判断新建或者保存的名称是否与已存在的名称有重复
                            var dashboards = this.dashboardServ.getAll();
                            for (var i = 0; i < dashboards.length; i++) {
                                var dashboard = dashboards[i];
                                //因为View的input是绑定到了tempDashboard.name，如果不加上不是同一个widgetId判断，这个判断永远为true
                                if (dashboard.id !== this.tempDashboard.id && dashboard.name === this.tempDashboard.name) {
                                    this.warnMessage = '仪表盘名称已存在';
                                    return;
                                }
                            }
                            break;
                        case 'move':
                            if (this.tempDashboard.groupId === this.selectGroupId) {
                                this.warnMessage = '此仪表盘已属于 ' + this.groupName + ' 分组';
                                return;
                            }
                            break;
                        default:
                    }

                    this.tempDashboard.save(isCreate).then(function () {
                        //如果是复制到当前分组下，则需要往dashboardService中添加这个新复制的仪表盘

                        //如果是移动，需要从当前分组下删除dashboardService中该仪表盘

                        //保存成功后，通过事件冒泡的方式切换到新的仪表盘

                        //var index = this.dashboardServ.getIndexById(this.tempDashboard.id);
                        ////如果存在，则删除已有的，再将临时的添加进来。如果不存在，则直接添加
                        //if (index > -1) {
                        //    this.dashboardServ.del(index);
                        //}
                        ////将临时的仪表盘对象添加到groupService中
                        //this.dashboardServ.add(this.tempDashboard);

                        //
                        //this.$dispatch('dashboardEdit.switchStep', 'chart-configure')
                    }.bind(this));
                },
                cancel: function () {
                    //放弃仪表盘复制与移动，应该返回到当前仪表盘的编辑
                    this.$dispatch('chartConfigure.closeWidgetConfigure');
                }
            }
        });


    })