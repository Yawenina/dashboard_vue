define(['vue', 'text!template/appContent/dashboardEdit/chartConfigure/widgetConfigure.html',
        'component/appContent/dashboardEdit/chartConfigure/widgetConfigure/datasourceConfigure',
        'component/appContent/dashboardEdit/chartConfigure/widgetConfigure/dimensionIndicator'
],
    function (Vue, tmpl, datasourceConfigureComp, dimensionIndicatorComp) {

        return Vue.component('widget-configure', {
            template: tmpl,
            components: {
                'datasource-configure': datasourceConfigureComp,
                'dimension-indicator': dimensionIndicatorComp
            },
            data: function () {
                return {
                    passWidget: null,
                    tabs: [
                        { name: '数据源配置', componentName: 'datasource-configure' },
                        { name: '维度指标配置', componentName: 'dimension-indicator' }
                    ],
                    currentTabIndex: -1,
                    loadModalComponentName: '',
                    selectDimensionAndIndicators: {}    //保存选择的维度指标（根据数据源选项生成key——selectDimAndIndKey）
                }
            },
            computed: {
                /**
                 * 获取存储已选择的维度指标对象的key
                 * @returns {String} key值
                 */
                selectDimAndIndKey: function () {
                    return this.passWidget.id + '_' + this.passWidget.productId + '_' + this.passWidget.profileId + '_' + this.passWidget.apiType;
                },
                /**
                 * 选择的数据源选项所对应的，已选择的维度指标
                 */
                currentSelectDimAndInd: {
                    get: function () {
                        var selectDimAndInd = this.selectDimensionAndIndicators[this.selectDimAndIndKey];
                        if (!selectDimAndInd) {
                            //赋予已选择的维度指标对象初始值
                            selectDimAndInd = this.selectDimensionAndIndicators[this.selectDimAndIndKey] = {};
                            ['selectDimension', 'selectIndicator', 'selectFilterCondition', 'selectOrderBy', 'selectTopCount'].forEach(
                                function (selectKey) {
                                    if (selectKey === 'selectTopCount') {
                                        this[selectKey] = 0;
                                    } else {
                                        this[selectKey] = [];
                                    }
                                }.bind(selectDimAndInd));
                        }
                        return selectDimAndInd;
                    },
                    set: function (value) {
                        this.selectDimensionAndIndicators[this.selectDimAndIndKey] = value;
                    }
                }
            },
            methods: {
                isCurrentTab: function (index) {
                    return this.currentTabIndex === index;
                },
                switchTab: function (index) {
                    //如果当前widget已初始化，则通过事件广播的方式通知widget，否则通过widget的生命周期通知widget初始化
                    if (this.isCurrentTab(index)) {
                        //通过事件传播的方式通知相应widget的initComponent事件
                        this.$broadcast(this.tabs[index].componentName.replace(/-\w/, function (char) {
                            return char.replace('-', '').toUpperCase();
                        }) + '.initComponent', this.passWidget);
                    } else {
                        this.currentTabIndex = index;
                        this.loadModalComponentName = this.tabs[index].componentName;
                    }
                },
                closeWidgetConfigure: function () {

                    //将选中的数据源选项所对应的，已选择的维度指标赋给当前widget
                    for (var propName in this.currentSelectDimAndInd) {
                        if (this.currentSelectDimAndInd.hasOwnProperty(propName)) {
                            this.passWidget.config[propName] = this.currentSelectDimAndInd[propName];
                        }
                    }

                    //关闭配置窗口
                    this.$dispatch('chartConfigure.closeWidgetConfigure');
                }
            },
            events: {
                'widgetConfigure.startConfigure': function (widget) {
                    //将‘noModify’状态修改为‘update’
                    if (widget.config.status === 'noModify') {
                        widget.config.status = 'update';
                    }
                    this.passWidget = widget;

                    /**
                     * 因为这个widget创建后并不销毁，仅仅是modal隐藏，所以需要判断是否已记录过选择的维度指标
                     * 如果没有记录过，根据当前widget中所选的数据源选项生成key，并保存已选择的维度和指标
                     */
                    if (!this.selectDimensionAndIndicators[this.selectDimAndIndKey]) {
                        for (var selectKey in this.currentSelectDimAndInd) {
                            if (selectKey === 'selectTopCount') {
                                this.currentSelectDimAndInd[selectKey] = this.passWidget.config[selectKey];
                            } else {
                                var oneItem = this.currentSelectDimAndInd[selectKey];
                                this.passWidget.config[selectKey].forEach(function (selectValue) {
                                    this.push(selectValue);
                                }.bind(oneItem));
                            }
                        };
                    }

                    //切换到第一个选项卡
                    this.switchTab(0);
                },
                'widgetConfigure.switchConfigure': function (tabIndex) {
                    this.switchTab(tabIndex);
                }
            }
        });

    })