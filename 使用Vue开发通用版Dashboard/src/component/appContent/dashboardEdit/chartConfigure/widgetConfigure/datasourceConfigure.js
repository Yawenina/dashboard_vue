﻿define(['vue', 'text!template/appContent/dashboardEdit/chartConfigure/widgetConfigure/datasourceConfigure.html',
    'component/appContent/dashboardEdit/chartConfigure/widgetConfigure/datasourceConfigure_AD',
    'component/appContent/dashboardEdit/chartConfigure/widgetConfigure/datasourceConfigure_SEMD',
    'service/datasource'],
    function (Vue, tmpl, datasourceConfigure_AD, datasourceConfigure_SEMD, datasourceConfigureServ) {

        return Vue.component('datasource-configure', {
            template: tmpl,
            components: {
                'datasource-configure_AD': datasourceConfigure_AD,
                'datasource-configure_SEMD': datasourceConfigure_SEMD
            },
            props: {
                tempWidget: {
                    type: Object,
                    required: true
                }
            },
            data: function () {
                return {
                    dsServ: datasourceConfigureServ,
                    products: null,
                    solutions: null,
                    profiles: null,
                    apiTypes: null,
                    apiTypeDetailCompName: ''
                }
            },
            computed: {
                selectProduct: {
                    get: function () {
                        return this.tempWidget.productId.toString();
                    },
                    set: function (value) {
                        this.tempWidget.productId = this.dsServ.product.getById(value).id;
                        //触发菜单联动
                        this.menuLinkage('product', 'solution');

                        //根据不同产品，改变报表类型
                        this.apiTypes = this.dsServ.product.getById(value).apiType;
                        //如果是新建widget，或者从某个产品的报表，切换到另外一个产品，但没有上一个产品的报表类型，此时默认选择第一个报表类型
                        if (this.tempWidget.apiType > 0 && this.apiTypes[this.tempWidget.apiType]) {
                            this.selectApiType = this.tempWidget.apiType;
                        } else {
                            this.selectApiType = 1;
                        }                         
                    }
                },
                selectSolution: {
                    get: function () {
                        //solution对象的key是由product+solution两个id拼接的
                        return this.selectProduct + '' + this.tempWidget.solutionId;
                    },
                    set: function (value) {
                        this.tempWidget.solutionId = this.dsServ.solution.getById(value).id;
                        //触发菜单联动
                        this.menuLinkage('solution', 'profile');
                    }
                },
                selectProfile: {
                    get: function () {
                        //profile对象的key是由product+solution+profile三个id拼接的
                        return this.selectSolution + '' + this.tempWidget.profileId;
                    },
                    set: function (value) {
                        this.tempWidget.profileId = this.dsServ.profile.getById(value).id;

                        //通知推荐账户列表更新
                        this.$broadcast('datasourceConfigure_SEMD.changeAccountList', 1);

                        //通知自定义分组列表更新
                        this.$broadcast('datasourceConfigure_SEMD.changeAccountList', 2);
                    }
                },
                selectApiType: {
                    get: function () {
                        return this.tempWidget.apiType.toString();
                    },
                    set: function (value) {
                        this.tempWidget.apiType = Number(value);
                        //AD产品的频次报表有报表详细内容
                        if (this.selectProduct === '1024' && this.tempWidget.apiType === 2) {
                            this.apiTypeDetailCompName = 'datasource-configure_AD';
                        }
                        //SEMD产品有特殊的报表类型
                        else if (this.selectProduct === '4' && this.tempWidget.apiType === 1) {
                            this.apiTypeDetailCompName = 'datasource-configure_SEMD';
                        } else {
                            //解决从特殊报表类型切换回来以后，特殊报表选项不消失的问题
                            this.apiTypeDetailCompName = '';
                        }

                        //如果选了特殊报表类型，且widget中不存在特殊报表选项，则赋给一个空对象
                        if (this.apiTypeDetailCompName.length > 0 && !this.tempWidget.config.specialApiTypeOption) {
                            this.tempWidget.config.specialApiTypeOption = {}
                        }
                    }
                }
            },
            methods: {
                getSelectPropName: function (propName) {
                    return 'select' + propName.replace(/\w/, function (char) {
                        return char.toUpperCase();
                    });
                },
                /**
                 * 菜单联动
                 * @param higherLevelName 上一级数据项名称
                 * @param nextLevelName 下一级数据项名称
                 */
                menuLinkage: function (higherLevelName, nextLevelName) {
                    //记录选择的数据项
                    //this.higherLevelObj.selected = newValue;

                    if (!nextLevelName) {
                        return;
                    }
                    //重新给下一级数据项菜单赋值，实现下拉菜单联动效果
                    this.addDatasourceOptional_NextLevelAssign(higherLevelName, nextLevelName);
                },
                /**
                 * 添加数据项，如果添加的数据项中有nextLevel属性，则自动递归添加下一级数据项，并增加联动效果
                 * @param optionalName 数据项名称
                 */
                addDatasourceOptional: function (optionalName) {
                    var assemblePropName = optionalName + 's';
                    var selectPropName = this.getSelectPropName(optionalName);
                    var optionalObj = this.dsServ[optionalName];

                    this[assemblePropName] = optionalObj.getAll();
                },
                /**
                 * 递归创建下一级数据源菜单
                 * @param higherLevelName 上一级数据项的名称
                 * @returns {} 
                 */
                addDatasourceOptional_NextLevel: function (higherLevelName) {
                    var higherLevelObj = this.dsServ[higherLevelName];
                    if (!higherLevelObj) {
                        return;
                    }

                    var higherLevelSelectPropName = this.getSelectPropName(higherLevelName);
                    var higherLevelSelectedObj = higherLevelObj.getById(this[higherLevelSelectPropName]);
                    if (!higherLevelSelectedObj) {
                        return;
                    }

                    var currentLevelName = higherLevelSelectedObj.nextLevel;

                    if (!currentLevelName) {
                        return;
                    }
                    //给当前数据项赋值
                    this.addDatasourceOptional_NextLevelAssign(higherLevelName, currentLevelName);

                    this.addDatasourceOptional_NextLevel(currentLevelName);
                },
                /**
                 * 给下一级数据项的集合与选中的值赋值
                 * @param higherLevelObj dsServ里的对象
                 * @param nextLevelName 下一级数据项名称 
                 */
                addDatasourceOptional_NextLevelAssign: function (higherLevelName, nextLevelName) {

                    var higherLevelSelectPropName = this.getSelectPropName(higherLevelName);
                    var higherLevelObj = this.dsServ[higherLevelName];

                    //当前数据项集合名称
                    var assemblePropName = nextLevelName + 's';
                    //当前选中数据项的名称
                    var selectPropName = this.getSelectPropName(nextLevelName);
                    var nextLevelObj = this.dsServ[nextLevelName];

                    var nextLevelIdList = higherLevelObj.getNextLevelById(this[higherLevelSelectPropName]);
                    this[assemblePropName] = nextLevelObj.getById(nextLevelIdList);

                    //如果当前数据项集合不含有widget对应的值，默认第一个
                    if (!this[assemblePropName][this[selectPropName]]) {
                        this[selectPropName] = nextLevelIdList[0];
                    } else {
                        //触发set方法，实现菜单联动
                        this[selectPropName] = this[selectPropName];
                    }
                },
                product_onChange: function (productId) {
                    this.selectProduct = productId;
                },
                isCurrent: function (product, id) {
                    var selectPropName = this.getSelectPropName(product);
                    return this[selectPropName] === id;
                },
                //nextStep: function () {
                //    ['product', 'solution', 'profile', 'apiType'].forEach(function (propName) {
                //        var selectPropName = this.getSelectPropName(propName);
                //        this.tempWidget[propName !== 'apiType' ? propName + 'Id' : propName] = this[selectPropName];
                //    }.bind(this));

                //    this.$dispatch('widgetConfigure.switchConfigure', 1);
                //},
                cancel: function () {
                    this.$dispatch('chartConfigure.closeWidgetConfigure');
                }
            },
            events: {
                'datasourceConfigure.initComponent': function (widget) {

                    //如果从A组件的数据源配置切换到B组件的数据源配置，此时tempWidget不会发生变化，
                    //这种情况下，是通过事件广播的方式进行通知，而不是通过组件的生命周期函数通知，
                    //因此，如果传递过来widget参数，此widget才是当前配置的widget
                    if (widget && this.tempWidget.id !== widget.id) {
                        this.tempWidget = widget;
                    }

                    var deferred = $.Deferred();
                    if (!this.dsServ.product) {
                        deferred = this.dsServ.requestAll();
                    } else {
                        deferred.resolve();
                    }

                    $.when(deferred).done(function() {

                        //['product', 'solution', 'profile', 'apiType'].forEach(function (propName) {
                        //    this[this.getSelectPropName(propName)] = this.tempWidget[propName !== 'apiType' ? propName + 'Id' : propName];
                        //}.bind(this));

                        //因为product、solution、profile之间有联动关系（这种关系通过返回的数据中nextLevel明确）
                        //所以直接增加product数据项即可。
                        this.addDatasourceOptional('product');

                        //如果是新建widget，默认选择AD产品，通过菜单联动，可以设置solution与profile默认都为第一个
                        this.selectProduct = this.tempWidget.productId > 0 ? this.tempWidget.productId : '1024';
                    }.bind(this));
                }
            },
            created: function () {
                this.$emit('datasourceConfigure.initComponent');
            }
        });

    })