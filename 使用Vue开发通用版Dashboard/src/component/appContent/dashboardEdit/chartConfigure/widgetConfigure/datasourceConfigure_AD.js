﻿define(['vue', 'text!template/appContent/dashboardEdit/chartConfigure/widgetConfigure/datasourceConfigure_AD.html'],
    function (Vue, tmpl) {

        return Vue.component('datasource-configure_AD', {
            template: tmpl,
            props: {
                tempWidget: {
                    type: Object,
                    required: true
                }
            },
            data: function() {
                return {                    
                    timesizes: [
                        {
                            name: '整体',
                            value: 'None'
                        },
                        {
                            name: '小时',
                            value: 'Hour'
                        },
                        {
                            name: '天',
                            value: 'Day'
                        },
                        {
                            name: '周',
                            value: 'Week'
                        },
                        {
                            name: '月',
                            value: 'Month'
                        }
                    ],                    
                    distinctFields: [
                        {
                            name: 'Cookie去重',
                            value: 'Cookie'
                        },
                        {
                            name: 'IP地址去重',
                            value: 'IpAddress'
                        }
                    ]                                       
                }
            },
            computed: {
                selectTimeSize: {
                    get: function () {
                        var interval = this.tempWidget.config.specialApiTypeOption.Interval;
                        if (!interval) {
                            interval = this.tempWidget.config.specialApiTypeOption.Interval = 'None';
                        }
                        return interval;
                    },
                    set: function(value) {
                        this.tempWidget.config.specialApiTypeOption.Interval = value;
                    }
                },
                selectDistinctBy: {
                    get: function () {
                        var distinctBy = this.tempWidget.config.specialApiTypeOption.DistinctBy;
                        if (!distinctBy) {
                            distinctBy = this.tempWidget.config.specialApiTypeOption.DistinctBy = 'Cookie';
                        }
                        return distinctBy;
                    },
                    set: function (value) {
                        this.tempWidget.config.specialApiTypeOption.DistinctBy = value;
                    }
                },
                isNonFrequencyLimit: {
                    get: function () {
                        var frequencyLimit = this.tempWidget.config.specialApiTypeOption.FrequencyLimit;
                        if (!frequencyLimit) {
                            return 'true';
                        }
                        return 'false';
                    },
                    set: function (value) {
                        if (value === 'false') {
                            this.tempWidget.config.specialApiTypeOption.FrequencyLimit = this.setFrequencyLimit;
                        } else {
                            delete this.tempWidget.config.specialApiTypeOption.FrequencyLimit;
                        }
                    }
                },
                setFrequencyLimit: {
                    get: function () {
                        var frequencyLimit = this.tempWidget.config.specialApiTypeOption.FrequencyLimit;
                        if (!frequencyLimit) {
                            frequencyLimit = 21;
                        }
                        return frequencyLimit;
                    },
                    set: function (value) {
                        if (this.isNonFrequencyLimit === 'false') {
                            this.tempWidget.config.specialApiTypeOption.FrequencyLimit = value;
                        }
                    }
                }
            }
        });

    })