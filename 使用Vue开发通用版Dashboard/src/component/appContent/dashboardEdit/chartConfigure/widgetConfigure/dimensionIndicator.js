﻿define(['vue', 'text!template/appContent/dashboardEdit/chartConfigure/widgetConfigure/dimensionIndicator.html',
    'component/tree/treeItem', 'service/metaData', 'service/availableWidget'],
    function (Vue, tmpl, treeItemComp, metaDataServ, availableWidgetServ) {

        return Vue.component('dimension-indicator', {
            template: tmpl,
            components: {
                'tree-item': treeItemComp
            },
            props: {
                tempWidget: {
                    type: Object,
                    required: true
                },
                currentSelectDimAndInd: {
                    type: Object,
                    required: true
                }
            },
            data: function () {
                return {
                    metaDataServ: metaDataServ,
                    availableWidgetServ: availableWidgetServ,
                    dimensions: {},
                    indicators: {}
                }
            },
            computed: {
                isNonRectangular: function () {
                    return this.availableWidgetServ.getByName(this.tempWidget.config.widgetName).isNonRectangular;
                },
                selectDimension: function () {
                    return this.currentSelectDimAndInd.selectDimension;
                },
                selectIndicator: function () {
                    return this.currentSelectDimAndInd.selectIndicator;
                },
                selectFilterCondition: function () {
                    return this.currentSelectDimAndInd.selectFilterCondition;
                },
                selectOrderBy: function () {
                    return this.currentSelectDimAndInd.selectOrderBy;
                },
                selectTopCount: {
                    get: function () {
                        var topCount = this.currentSelectDimAndInd.selectTopCount;
                        //如果未设置显示数量，界面上不显示0，显示input的提示信息
                        if (topCount < 1) {
                            topCount = '';
                        }
                        return topCount;
                    },
                    set: function (value) {
                        var topCount = Number(value);
                        if (isNaN(topCount) || topCount < 1) {
                            topCount = 0;
                        }
                        this.currentSelectDimAndInd.selectTopCount = topCount;
                    }
                }
            },
            methods: {
                deleteSelectItem: function (propName, index) {
                    this[propName].splice(index, 1);
                },
                /**
                 * 将数据转换成树形组件所支持的JSON格式
                 */
                convertToTreeData: function (response) {
                    var children = this.createLeafNode(response);
                    
                    if (this.tempWidget.productId === 4) {
                        //SEMD产品获取的MetaData有维度与指标两个根节点
                        this.dimensions = children.dimensions[0];
                        this.indicators = children.indicators[0];
                    } else {
                        this.dimensions = {
                            name: '维度',
                            children: children.dimensions
                        };
                        this.indicators = {
                            name: '指标',
                            children: children.indicators
                        }
                    }
                },
                createLeafNode: function (nodes) {
                    var dimensions = [],
                        indicators = [];
                    for (var i = 0; i < nodes.length; i++) {
                        var node = nodes[i];
                        var oneLeaf = {
                            name: node.name
                        };
                        var childrenNodes = node.nodes;
                        if (!childrenNodes || childrenNodes.length === 0) {
                            childrenNodes = node.leaves;
                        }
                        if (childrenNodes && childrenNodes.length > 0) {
                            var childrenLeaf = this.createLeafNode(childrenNodes);
                            if (childrenLeaf.dimensions.length > 0) {
                                oneLeaf.children = childrenLeaf.dimensions;
                                dimensions.push(oneLeaf);
                            } else {
                                oneLeaf.children = childrenLeaf.indicators;
                                indicators.push(oneLeaf);
                            }
                        } else {
                            oneLeaf.key = node.key;
                            //如果是WD产品，判断是否是维度，以是否可以过滤为判断原则
                            if (this.tempWidget.productId === 1) {
                                if ((node.columnTypes & 2) === 2) {
                                    dimensions.push(oneLeaf);
                                } else {
                                    indicators.push(oneLeaf);
                                }
                            } else {
                                //如果columnTypes与8的结果为8，表示能被grouped，即当前节点是维度
                                if ((node.columnTypes & 8) === 8) {
                                    dimensions.push(oneLeaf);
                                } else {
                                    indicators.push(oneLeaf);
                                }
                            }
                        }
                    }

                    return {
                        'dimensions': dimensions,
                        'indicators': indicators
                    };
                },
                getIndexByKey: function (propName, key) {
                    if (this[propName]) {
                        for (var i = 0; i < this[propName].length; i++) {
                            if (this[propName][i].key === key) {
                                return i;
                            }
                        }
                    }
                    return -1;
                }
            },
            events: {
                'dimensionIndicator.initComponent': function (widget) {

                    //如果从A组件的数据源配置切换到B组件的数据源配置，此时tempWidget不会发生变化，
                    //这种情况下，是通过事件广播的方式进行通知，而不是通过组件的生命周期函数通知，
                    //因此，如果传递过来widget参数，此widget才是当前配置的widget
                    if (widget && this.tempWidget.id !== widget.id) {
                        this.tempWidget = widget;
                    }

                    var isNeedGetData = false;
                    if (!this.dimensions || !this.indicators) {
                        isNeedGetData = true;
                    } else {
                        //如果当前选择的数据源与widget的数据源一致，则不需要重新获取数据
                        ['product', 'solution', 'profile', 'apiType'].forEach(function (propName) {
                            propName = propName !== 'apiType' ? propName + 'Id' : propName;
                            if (this[propName] !== this.tempWidget[propName]) {
                                isNeedGetData = true;
                            }
                        }.bind(this));
                    }

                    if (isNeedGetData) {
                        var needAddOptionPairs = [];
                        //SEMD产品获取MetaData时需要添加特殊选项
                        if (this.tempWidget.productId === 4) {
                            var specialApiTypeOption = this.tempWidget.config.specialApiTypeOption;
                            for (var propName in specialApiTypeOption) {
                                if (!specialApiTypeOption.hasOwnProperty(propName)) {
                                    continue;
                                }
                                needAddOptionPairs.push({
                                    key: propName,
                                    value: specialApiTypeOption[propName]
                                });
                            }
                        }

                        this.metaDataServ.requestAll(this.tempWidget, needAddOptionPairs).then(function (response) {
                            this.convertToTreeData(response);
                        }.bind(this));
                    }
                }
            },
            created: function () {
                this.$emit('dimensionIndicator.initComponent');
            },
            ready: function () {
                var $element = $(this.$el).find('.selected-row, .selected-col, .selected-filter, .selected-orderBy');

                //利用jQuery UI的droppable增加拖放功能
                $element.droppable({
                    //只有draggable具有相同scope的元素可拖放
                    scope: 'treeItem.widgetDragAndDrop'
                });

                $element.on('drop', this, function (event, ui) {
                    var _this = event.data;
                    var $dropItem = ui.helper;

                    var dropItem = {
                        key: $dropItem.attr('key'),
                        name: $dropItem.text()
                    };
                    if ($(this).hasClass('selected-row')) {
                        var selectDimension = _this.selectDimension;
                        if (_this.getIndexByKey('selectDimension', dropItem.key) === -1) {
                            selectDimension.push(dropItem);
                        }
                    } else if ($(this).hasClass('selected-col')) {
                        var selectIndicator = _this.selectIndicator;
                        if (_this.getIndexByKey('selectIndicator', dropItem.key) === -1) {
                            selectIndicator.push(dropItem);
                        }
                    }
                    else if ($(this).hasClass('selected-filter')) {
                        var selectFilterCondition = _this.selectFilterCondition;
                        if (_this.getIndexByKey('selectFilterCondition', dropItem.key) === -1) {
                            //默认过滤条件类型是等于
                            dropItem.type = 'Equal';
                            dropItem.value = '';
                            selectFilterCondition.push(dropItem);
                        }
                    }
                    else if ($(this).hasClass('selected-orderBy')) {
                        var selectOrderBy = _this.selectOrderBy;
                        if (_this.getIndexByKey('selectOrderBy', dropItem.key) === -1) {
                            //默认升序
                            dropItem.direction = 0;
                            selectOrderBy.push(dropItem);
                        }
                    }
                });
            }
        });

    })