﻿define(['vue', 'text!template/appContent/dashboardEdit/chartConfigure/widgetConfigure/datasourceConfigure_SEMD.html',
        'component/tree/treeItem', 'service/datasource'
],
    function (Vue, tmpl, treeItemComp, dsServ) {

        return Vue.component('datasource-configure_SEMD', {
            template: tmpl,
            components: {
                treeItem: treeItemComp
            },
            props: {
                tempWidget: {
                    type: Object,
                    required: true
                }
            },
            data: function () {
                return {
                    dsServ: dsServ,
                    medias: [
                        {
                            name: '百度搜索',
                            value: 'BaiduFengchao'
                        },
                        {
                            name: '百度网盟',
                            value: 'BaiduBeidou'
                        },
                        {
                            name: '360搜索',
                            value: 'Search360'
                        },
                        {
                            name: '搜狗搜索',
                            value: 'SogouSearch'
                        },
                        {
                            name: '神马搜索',
                            value: 'SmWolong'
                        }
                    ],
                    accountTypes: [
                        {
                            name: '推广账户',
                            value: 'Account'
                        },
                        {
                            name: '自定义分组',
                            value: 'CustomGroup'
                        }
                    ],
                    queryTypes: [
                        {
                            name: '推广账户',
                            value: 'Account'
                        },
                        {
                            name: '推广计划',
                            value: 'Campaign'
                        },
                        {
                            name: '推广单元',
                            value: 'Adgroup'
                        },
                        {
                            name: '关键词',
                            value: 'Keyword',
                            visible: true
                        },
                        {
                            name: '创意',
                            value: 'Ad'
                        },
                        {
                            name: '搜索关键词',
                            value: 'SearchQuery',
                            visible: true
                        }
                    ],
                    deviceTypes: [
                        {
                            name: '全部设备',
                            value: 'All'
                        },
                        {
                            name: '计算机',
                            value: 'PC'
                        },
                        {
                            name: '移动',
                            value: 'Mobile'
                        }
                    ],
                    isIncludeDeletes: [
                        {
                            name: '是',
                            value: '1'
                        },
                        {
                            name: '否',
                            value: '0'
                        }
                    ],
                    dateTimeSeriesTypes: [
                        {
                            name: '无',
                            value: ''
                        },
                        {
                            name: '天',
                            value: 'ByDay'
                        },
                        {
                            name: '小时',
                            value: 'ByHour'
                        }
                    ],
                    isEnableSelect: {
                        DeviceType: true,
                        DateTimeSeriesType: true
                    },
                    accountList: {},
                    accounts: [],
                    selectAccounts: {},
                    selectDeviceType: '',
                    selectDateTimeSeriesType: ''
                }
            },
            computed: {
                selectOption: function () {
                    //设置特殊选项的默认值
                    var defaultOption = {
                        AccountType: 'BaiduFengchao',
                        GroupType: 'Account',
                        GroupKeys: '',
                        QueryObjectType: 'Account',
                        DeviceType: 'All',
                        IsDeleted: 0,
                        //DateTimeSeriesType: ''    //时间粒度默认是‘无’，请求时不需要添加DateTimeSeriesType，所以默认值里没有这个选项
                    };

                    //如果已保存组件中没有特殊选项，则赋予默认值
                    var saveOption = this.tempWidget.config.specialApiTypeOption;
                    for (var propName in defaultOption) {
                        if (saveOption.hasOwnProperty(propName)) {
                            continue;
                        }
                        saveOption[propName] = defaultOption[propName];
                    }

                    return saveOption;
                },
                selectMedia: {
                    get: function () {

                        return this.selectOption.AccountType;
                    },
                    set: function (value) {

                        this.isEnableSelect.DeviceType = false;
                        //请求数据时，不需要多余的参数
                        delete this.selectOption.DeviceType;
                        this.selectDeviceType = 'All';

                        this.queryTypes[3].visible = true;
                        this.queryTypes[5].visible = true;

                        switch (value) {
                            case 'BaiduFengchao':
                                //如果媒体选择了百度搜索，设备类型才可选
                                this.isEnableSelect.DeviceType = true;
                                break;
                            case 'BaiduBeidou':
                                //如果媒体选择了百度网盟，报表类型中没有关键词和搜索关键词选项
                                this.queryTypes[3].visible = false;
                                this.queryTypes[5].visible = false;
                                break;
                            case 'SmWolong':
                                //如果媒体选择了神马搜索，报表类型中没有搜索关键词选项
                                this.queryTypes[5].visible = false;
                                break;
                        }

                        if (this.selectOption.AccountType === value) {
                            return;
                        }
                        this.selectOption.AccountType = value;

                        //通知推荐账户列表更新
                        this.$emit('datasourceConfigure_SEMD.changeAccountList', 1);

                        //通知自定义分组列表更新
                        this.$emit('datasourceConfigure_SEMD.changeAccountList', 2);
                    }
                },
                selectQueryType: {
                    get: function () {

                        return this.selectOption.QueryObjectType;
                    },
                    set: function (value) {
                        //如果报表类型选择了“搜索关键词”，时间粒度设为不可选
                        if (value === 'SearchQuery') {
                            this.isEnableSelect.DateTimeSeriesType = false;
                            this.selectDateTimeSeriesType = '';
                            //请求数据时，不需要多余的参数
                            delete this.selectOption.dateTimeSeriesTypes;
                        } else {
                            this.isEnableSelect.DateTimeSeriesType = true;
                        }

                        this.selectOption.QueryObjectType = value;
                    }
                }
            },
            methods: {
                convertMediaNameToRequestParamId: function (mediaName) {
                    if (!mediaName) {
                        mediaName = this.selectOption.GroupType;
                    }
                    if (mediaName === 'Account') {
                        return 1;
                    }
                    else if (mediaName === 'CustomGroup') {
                        return 2;
                    }
                    return 0;
                },
                copySelectAccountObj: function (addKey, addName) {

                    /**
                     * 遇到的问题：在jQuery的事件里，更新Vuejs的data里，给对象添加属性，DOM不更新
                     * 原先的代码：_this.selectAccounts[dropItem.key] = dropItem.name;
                     * 解决的方案：
                     *  方案一、给这个对象重新赋予一个新实例化的对象，这样就能触发Vuejs的DOM更新
                     *    缺点：需要一个for循环对新的对象赋值
                     *  方案二、不使用对象，改用Array，push的时候就会触发DOM更新。因为Vuejs重写了Array的方法
                     *    缺点：原有对象的通过key查找功能无效，需要通过for循环遍历数组查找
                     */
                    var tempSelectAccounts = {};
                    for (var key in this.selectAccounts) {
                        tempSelectAccounts[key] = this.selectAccounts[key];
                    }
                    tempSelectAccounts[addKey] = addName;
                    this.selectAccounts = tempSelectAccounts;
                },
                addDraggableEvent: function () {
                    //设置推广账户列表能够拖放，因为View上的内容是随着accounts变化的，而且accounts是通过异步获取的
                    //在ready中，accounts内容为空，find('.canDragAccountItem')是找不到任何元素的，所以绑不了drag事件
                    //将绑定事件单独抽出来，在$nextTick中实现绑定数据
                    this.$nextTick(function () {
                        $(this.$el).find('.canDragAccountItem').draggable({
                            //只有与UILayout的droppable具有相同scope的元素才可拖放
                            scope: 'treeItem.widgetDragAndDrop',
                            helper: 'clone',
                            appendTo: 'body', //因为有overflow，不把拖动的元素从div中拿出，是拖不出来的
                            zIndex: 999999999
                        });
                    });
                },
                updateAccountList: function (paramId, isRecoverSelectedList) {

                    this.selectAccounts = {};
                    //是否要恢复已选择的推广账户/自定义分组列表
                    if (!isRecoverSelectedList) {
                        this.selectOption.GroupKeys = '';
                    }

                    var commonQueryParam = this.tempWidget.dsConvertToParam();
                    return this.dsServ.requestAccountList(commonQueryParam, paramId).then(function (result) {

                        var _this = this.context,
                            selectedAccountKey = _this.selectOption.GroupKeys.split(',');

                        if (this.paramId === 1) {
                            for (var i = 0; i < result.Items.length; i++) {
                                var item = result.Items[i];
                                for (var propName in item) {
                                    if (item.hasOwnProperty(propName)) {
                                        var newPropName = propName.replace(/\w/, function (char) {
                                            return char.toLowerCase();
                                        });
                                        //如果属性名已经小写，则不处理
                                        if (newPropName === propName) {
                                            continue;
                                        }
                                        item[newPropName] = item[propName];
                                        delete item[propName];
                                    }
                                }

                                //还原已选择的账户列表/自定义分组列表
                                if (selectedAccountKey.indexOf(item.key.toString()) > -1) {
                                    _this.copySelectAccountObj(item.key, item.name);
                                }
                            }

                            _this.accountList[this.paramId] = result.Items;

                        } else {
                            var customGroup = result[_this.selectMedia];
                            var groupList = [];

                            for (var i = 0; customGroup && i < _this.queryTypes.length; i++) {
                                var queryType = _this.queryTypes[i];

                                var oneCustomGroup = customGroup[queryType.value];
                                if (!oneCustomGroup) {
                                    continue;
                                }

                                var oneGroupList = [];
                                for (var j = 0; j < oneCustomGroup.length; j++) {
                                    var key = oneCustomGroup[j].GroupId,
                                        name = oneCustomGroup[j].Name;

                                    oneGroupList.push({
                                        key: key,
                                        name: name
                                    });

                                    //还原已选择的账户列表/自定义分组列表
                                    if (selectedAccountKey.indexOf(key.toString()) > -1) {
                                        _this.copySelectAccountObj(key, name);
                                    }
                                }

                                groupList.push({
                                    key: queryType.value,
                                    name: queryType.name,
                                    children: oneGroupList
                                });
                            }

                            if (groupList.length > 0) {
                                _this.accountList[this.paramId] = {
                                    name: '自定义分组',
                                    children: groupList
                                };
                            }
                        }

                        var key = _this.convertMediaNameToRequestParamId();
                        //是否返回的数据是当前账户类型对应的列表，是的话，更新界面
                        if (key === this.paramId) {
                            _this.accounts = _this.accountList[key];
                            //绑定可拖动事件
                            _this.addDraggableEvent();
                        }

                    }.bind({
                        context: this,
                        paramId: paramId
                    }));
                },

                accountType_onChange: function () {
                    var propName = this.convertMediaNameToRequestParamId(event.target.value);
                    this.accounts = this.accountList[propName];
                    //绑定可拖动事件
                    this.addDraggableEvent();

                    this.selectAccounts = {};
                },
                deviceType_onChange: function () {
                    if (this.isEnableSelect.DeviceType) {
                        this.selectOption.DeviceType = event.target.value;
                    }
                },
                dateTimeSeriesType_onChange: function () {
                    var selectValue = event.target.value;
                    if (selectValue === '') {
                        delete this.selectOption.DateTimeSeriesType
                    }
                    else if (this.isEnableSelect.DateTimeSeriesType) {
                        this.selectOption.DateTimeSeriesType = selectValue;
                    }
                },
                deleteSelectAccount: function (key) {
                    if (!this.selectAccounts[key]) {
                        return;
                    }

                    //考虑到替换时出现11,被1,替换的情况，前后各增加了一个逗号来进行替换
                    var allKeys = ',' + this.selectOption.GroupKeys + ',';
                    this.selectOption.GroupKeys = allKeys.replace(',' + key + ',', '');

                    delete this.selectAccounts[key];
                }
            },
            events: {
                'datasourceConfigure_SEMD.changeAccountList': function (paramId) {
                    this.updateAccountList(paramId);
                }
            },
            ready: function () {

                var saveSelectDeviceType = this.selectOption.DeviceType;
                //调用set方法，设置不同媒体，对应不同的选项调整
                this.selectMedia = this.selectOption.AccountType;
                this.selectDeviceType = saveSelectDeviceType;

                var saveSelectDateTimeSeriesType = this.selectOption.DateTimeSeriesType;
                //调用set方法，设置不同报表类型，对应不同的选项调整
                this.selectQueryType = this.selectOption.QueryObjectType;
                this.selectDateTimeSeriesType = saveSelectDateTimeSeriesType ? saveSelectDateTimeSeriesType : '';

                //更新界面推广账户列表
                var key = this.convertMediaNameToRequestParamId();
                this.updateAccountList(key, true);



                var $element = $(this.$el).find('.canDropAccount');

                //利用jQuery UI的droppable增加拖放功能
                $element.droppable({
                    //只有draggable具有相同scope的元素可拖放
                    scope: 'treeItem.widgetDragAndDrop'
                });

                $element.on('drop', this, function (event, ui) {
                    var _this = event.data,
                        $dropItem = ui.helper,
                        dropItem = {
                            key: $dropItem.attr('key'),
                            name: $dropItem.text()
                        };

                    var account = _this.selectAccounts[dropItem.key];
                    if (account) {
                        return;
                    }

                    _this.copySelectAccountObj(dropItem.key, dropItem.name);


                    if (_this.selectOption.GroupKeys.length === 0) {
                        _this.selectOption.GroupKeys = dropItem.key;
                    } else {
                        _this.selectOption.GroupKeys += ',' + dropItem.key;
                    }
                });
            }
        });

    })