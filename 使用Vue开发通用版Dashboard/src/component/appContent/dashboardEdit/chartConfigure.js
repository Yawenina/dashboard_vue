﻿define(['vue', 'text!template/appContent/dashboardEdit/chartConfigure.html',
        'component/gridstack/gridstack', 'component/appContent/dashboardEdit/chartConfigure/widgetConfigure',
        'service/widget', 'service/availableWidget'],
    function (Vue, tmpl, gridstackComp, configureComp, widgetServ, availableWidgetServ) {

        return Vue.component('chart-configure', {
            template: tmpl,
            components: {
                'grid-stack': gridstackComp,
                'widget-configure': configureComp
            },
            props: {
                passDashboard: {
                    type: Object,
                    required: true
                }
            },
            data: function () {
                return {
                    widgetServ: widgetServ,
                    availableWidgetServ: availableWidgetServ,
                    widgets: [],
                    isShowModal: false,
                    passToModalWidget: null
                }
            },
            computed: {
                availableWidgetList: function () {
                    return this.availableWidgetServ.getAll();
                }
            },
            methods: {
                getImgSrc: function (availableWidget) {
                    return 'componentIcon/' + (availableWidget.icon || availableWidget.name + '.png');
                },
                /**
                 * 拖放到表格区域，并计算完新的layout之后，调用的handler
                 * @param $dropWidget 拖放的widget
                 * @param layout {x: 第几行, y: 第几列} 拖放的位置
                 * @return {Model.constructor.Widget} 新创建的widget
                 */
                widgetDropDown: function ($dropWidget, layout) {

                    var availableWidget = this.availableWidgetServ.getByName($dropWidget.attr('title'));

                    //如果设置了跨多少行多少列，则替换掉默认值
                    if (availableWidget.layout) {
                        layout.row = availableWidget.layout.row;
                        layout.col = availableWidget.layout.col;
                    }

                    //创建一个Widget对象，并按照拖放的元素初始化图表
                    var widget = this.widgetServ.createNew(this.passDashboard.id);
                    widget.chartType = availableWidget.chartType;
                    widget.config.widgetName = availableWidget.name;                    
                    widget.config.chartOption = availableWidget.chartOption;
                    widget.config.layout = layout;

                    return widget;
                }
            },
            events: {
                'chartConfigure.init': function () {

                    //通知gridstack小部件，创建布局
                    this.$broadcast('gridstack.createLayout');
                    
                    //因为每次切换仪表盘都是先走show，requestAllWidget方法就写在了dashboardShow里面，这里直接getAll就行
                    this.widgets = this.widgetServ.getAll();

                    //设置可用的小部件列表能够拖放
                    $(this.$el).find('.availableWidget img').draggable({
                        //只有与UILayout的droppable具有相同scope的元素才可拖放
                        scope: 'ChartConfigure.WidgetDragAndDrop',
                        helper: 'clone'
                    });
                },
                /**
                 * 打开配置指定widget窗口
                 */
                'chartConfigure.widgetConfigure': function (widget) {
                    //暂时先不克隆widget，因为widget配置界面中取消了保存图标，只有关闭图标
                    var widgetObjTemp = widget;
                    ////对要配置的widget进行clone
                    //var widgetObjTemp = this.widgetServ.createNew(this.passDashboard.id);
                    ////利用jQuery的extend方法，将widget对象克隆一份
                    //$.extend(true, widgetObjTemp, widget);

                    this.isShowModal = true;
                    this.$broadcast('widgetConfigure.startConfigure', widgetObjTemp);
                },
                'chartConfigure.closeWidgetConfigure': function () {
                    this.isShowModal = false;
                }
            },
            ready: function () {
                this.$emit('chartConfigure.init');                
            }
        });

    })