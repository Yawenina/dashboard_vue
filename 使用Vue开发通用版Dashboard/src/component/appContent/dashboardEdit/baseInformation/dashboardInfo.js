﻿define(['vue', 'component/modal',
        'text!template/appContent/dashboardEdit/baseInformation/dashboardInfo.html', 'service/group', 'service/dashboard'
    ],
    function (Vue, modalComp, dashboardInfoTmpl, groupServ, dashboardServ) {

        return Vue.component('dashboard-info', {
            template: dashboardInfoTmpl,
            props: {
                tempDashboard: {
                    type: Object,
                    required: true
                }
            },
            data: function () {
                return {
                    groupServ: groupServ,
                    dashboardServ: dashboardServ,
                    warnMessage: '',
                    selectGroup: null
                }
            },
            computed: {
                title: function () {
                    return '配置仪表盘——基本信息';
                },
                isDisableChangeGroup: function () {
                    //暂时关闭在新建仪表盘里调整分组
                    return true;
                    return this.tempDashboard.config.isDisableChangeGroup === true;
                },
                currentGroupName: {
                    get: function () {
                        if (!this.selectGroup) {
                            this.selectGroup = this.groupServ.get();
                        }
                        return this.selectGroup.name || '请新建一个分组';
                    },
                    set: function (value) {
                        this.selectGroup = value;
                    }
                },
                groups: function () {
                    return this.groupServ.getAll();
                },
                config: function () {
                    return this.tempDashboard.config;
                },
                filterLayoutRows: {
                    get: function () {
                        return this.getConfig('filterLayoutConfig', 'rows');
                    },
                    set: function (rows) {
                        this.setConfig('filterLayoutConfig', 'rows', rows);
                    }
                },
                filterLayoutCols: {
                    get: function () {
                        return this.getConfig('filterLayoutConfig', 'cols');
                    },
                    set: function (cols) {
                        this.setConfig('filterLayoutConfig', 'cols', cols);
                    }
                },
                uiLayoutRows: {
                    get: function () {
                        return this.getConfig('uiLayoutConfig', 'rows');
                    },
                    set: function (rows) {
                        this.setConfig('uiLayoutConfig', 'rows', rows);
                    }
                },
                uiLayoutCols: {
                    get: function () {
                        return this.getConfig('uiLayoutConfig', 'cols');
                    },
                    set: function (cols) {
                        this.setConfig('uiLayoutConfig', 'cols', cols);
                    }
                }
            },
            components: {
                modal: modalComp
            },
            methods: {
                /**
                 * 同一获取配置参数rows或者cols
                 * @param layoutName 区域名称
                 * @param propName 属性名（rows或者cols）
                 * @returns {number} 指定区域的rows或者cols值
                 */
                'getConfig': function (layoutName, propName) {
                    var layoutConfig = this.config[layoutName];
                    var defaultPropValue = 0;
                    if (layoutName.indexOf('ui') > -1) {
                        //图表展示区域默认是12行12列
                        defaultPropValue = 12;
                    }

                    if (!layoutConfig) {
                        layoutConfig = this.config[layoutName] = {};
                    }
                    //如果不存在的话，先设置一个默认值，这样用户直接点保存，就不会出现config中没有值的情况
                    if (!layoutConfig.hasOwnProperty(propName)) {
                        layoutConfig[propName] = defaultPropValue;
                    }
                    return layoutConfig[propName];
                },
                /**
                 * 统一设置配置参数的rows或cols
                 * @param layoutName 区域名称
                 * @param propName 属性名（rows或者cols）
                 * @param setValue 要设置的值
                 */
                'setConfig': function (layoutName, propName, setValue) {
                    var layoutConfig = this.config[layoutName],
                        propValue = Number(setValue);

                    //图表区域的行或者列临界值是1，必须有展示区域，最少有1行1列
                    var criticalValue = 1;
                    //如果是过滤区域的话，临界值是0，因为可以没有过滤区域
                    if (layoutName.indexOf('filter') > -1) {
                        criticalValue = 0;
                    }

                    if (propValue < criticalValue) {
                        console.error('dashboardInfo：%s的%s输入的值：%s不合法', layoutName, propName, setValue);
                        return;
                    }

                    if (!layoutConfig) {
                        layoutConfig = this.config[layoutName] = {};
                    }
                    layoutConfig[propName] = propValue;
                },
                'groupList_OnChange': function (groupId) {
                    //是否是新建组
                    if (groupId === 0) {
                        this.$dispatch('appContent.loadModalComponent', 'group-info', this.groupServ.createNew());
                        return;
                    }
                    this.tempDashboard.groupId = groupId;
                    this.selectGroup = this.groupServ.getById(groupId);
                },
                'saveDashboard': function () {

                    //判断新建或者保存的名称是否与已存在的名称有重复
                    var dashboards = this.dashboardServ.getAll();
                    for (var i = 0; i < dashboards.length; i++) {
                        var dashboard = dashboards[i];
                        //因为View的input是绑定到了tempDashboard.name，如果不加上不是同一个widgetId判断，这个判断永远为true
                        if (dashboard.id !== this.tempDashboard.id && dashboard.name === this.tempDashboard.name) {
                            this.warnMessage = '仪表盘名称已存在';
                            return;
                        }
                    }

                    var isCreate = false;
                    if (this.tempDashboard.id < 1) {
                        isCreate = true;
                    }

                    //如果行或者列有一个设置为0，删除掉相应的Layout，可以没有过滤区域，但必须有图表展示区域
                    var config = this.config.filterLayoutConfig;
                    if (config && (config.rows === 0 || config.cols === 0)) {
                        delete this.tempDashboard.config.filterLayoutConfig;
                    }

                    this.tempDashboard.save(isCreate).then(function () {
                        var index = this.dashboardServ.getIndexById(this.tempDashboard.id);
                        //如果存在，则删除已有的，再将临时的添加进来。如果不存在，则直接添加
                        if (index > -1) {
                            this.dashboardServ.del(index);
                        }
                        //将临时的仪表盘对象添加到groupService中
                        this.dashboardServ.add(this.tempDashboard);

                        //保存成功后，通过事件冒泡的方式调用dashboardEdit组件的切换流程事件
                        this.$dispatch('dashboardEdit.switchStep', 'chart-configure')
                    }.bind(this));
                },
                'cancelEdit': function () {
                    //放弃仪表盘基本信息编辑，应该返回到当前仪表盘的展示
                    this.$dispatch('appContent.switchView', 'dashboard-show', this.dashboardServ.get());
                }
            }
        });


    })