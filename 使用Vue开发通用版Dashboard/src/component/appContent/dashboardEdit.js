define(['vue', 'text!template/appContent/dashboardEdit.html', 'service/dashboard',
        'component/appContent/dashboardEdit/baseInformation', 'component/appContent/dashboardEdit/chartConfigure'
],
    function (Vue, dashboardEditTmpl, dashboardServ, baseInfoComp, chartConfigComp) {

        return Vue.component('dashboard-edit', {
            template: dashboardEditTmpl,
            props: {
                passDashboard: {
                    type: Object,
                    required: true
                }
            },
            data: function () {
                return {
                    currentStep: ''
                }
            },
            components: {
                'base-information': baseInfoComp,
                'chart-configure': chartConfigComp
            },
            events: {
                'dashboardEdit.switchDashboard': function () {
                    //第一步默认显示修改基本信息
                    this.$emit('dashboardEdit.switchStep', 'base-information');
                },
                'dashboardEdit.switchStep': function (componentName) {
                    //切换显示的仪表盘编辑步骤
                    this.$dispatch('callSiblingEvent', 'dashboardEditStep.switchShowStep', componentName);
                    this.currentStep = componentName;
                }
            },
            ready: function() {
                this.$emit('dashboardEdit.switchDashboard');
            }
        })
    })