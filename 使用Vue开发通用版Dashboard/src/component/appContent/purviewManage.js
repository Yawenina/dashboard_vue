﻿define(['vue', 'text!template/appContent/purviewManage.html',
        'component/appContent/purviewManage/userManage', 'component/appContent/purviewManage/roleManage'
    ],
    function (Vue, tmpl, userManageComp, roleManageComp) {

        return Vue.component('purview-manage', {
            template: tmpl,
            components: {
                'user-manage': userManageComp,
                'role-manage': roleManageComp
            }
        });

    })