define(['vue', 'text!template/appContent/dashboardEditStep.html'], function(Vue, tmpl) {

    return Vue.component('dashboard-edit-step', {
        template: tmpl,
        data: function () {
            return {
                currentStep: ''
            }
        },
        methods: {
            jumpToSpecifiedStep: function (stepName) {
                //通知仪表盘编辑组件，执行点击的仪表盘步骤
                this.$dispatch('callSiblingEvent', 'dashboardEdit.switchStep', stepName);
            }
        },
        events: {
            'dashboardEditStep.switchShowStep': function (stepName) {
                this.currentStep = stepName;
            }
        }
    });

})