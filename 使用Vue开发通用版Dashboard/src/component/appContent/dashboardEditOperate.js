﻿define(['vue', 'text!template/appContent/dashboardEditOperate.html',
        'service/dashboard', 'service/widget'],
    function (Vue, tmpl, dashboardServ, widgetServ) {

        return Vue.component('dashboard-edit-operate', {
            template: tmpl,
            data: function () {
                return {
                    dashboardServ: dashboardServ,
                    widgetServ: widgetServ
                }
            },
            computed: {
                widgets: function () {
                    return this.widgetServ.getAll();
                }
            },
            methods: {
                executeSpecifiedAction: function (action) {
                    switch (action) {
                        case 'save':
                            var isIncludeDateRangeFilter = false;
                            for (var i = 0; i < this.widgets.length; i++) {
                                var widget = this.widgets[i];

                                //是否包含日期范围过滤器，仪表盘中必须含有日期范围过滤器，因为通用查询需要
                                if (widget.chartType === 20010001) {
                                    isIncludeDateRangeFilter = true;
                                }
                            }
                            if (!isIncludeDateRangeFilter) {
                                alert('配置的仪表盘中必须包含日期范围过滤器！');
                                return;
                            }

                            var deferreds = [];
                            for (var i = 0; i < this.widgets.length; i++) {
                                var widget = this.widgets[i];

                                switch (widget.config.status) {
                                    case "create":
                                        deferreds.push(widget.save(true));
                                        break;         
                                    case "update":     
                                        deferreds.push(widget.save(false));
                                        break;         
                                    case "delete":     
                                        deferreds.push(widget.delete());
                                        break;
                                    default:
                                        continue;
                                }
                            }

                            $.when.apply($, deferreds).then(
                                function () {
                                    //改变所有widget的状态
                                    for (var i = 0; i < this.widgets.length; i++) {
                                        this.widgets[i].config.status = 'noModify';
                                    }

                                    //切换到展示当前仪表盘
                                    this.$dispatch('callSiblingEvent', 'appContent.switchView', 'dashboard-show', this.dashboardServ.get(), {alreadySave: true});
                                }.bind(this),
                                function () {
                                    console.error(arguments);
                                });

                            break;
                        case 'delete':
                            var dashboard = this.dashboardServ.get();
                            if (!dashboard) {
                                return;
                            }
                            if (!confirm('是否删除'+ dashboard.name +'仪表盘？')) {
                                return;
                            }
                            dashboard.delete().then(function () {
                                //从当前仪表盘服务中删除当前仪表盘对象
                                this.dashboardServ.del();
                                //del方法完成后，dashboardServ里的索引会自动减一，因此调用get方法，获取到的就是当前删除的上一个仪表盘
                                var dashboard = this.dashboardServ.get();                                
                                //切换展示界面
                                this.$dispatch('callSiblingEvent', 'appContent.switchView', 'dashboard-show', dashboard, { alreadySave: true });
                            }.bind(this));
                            break;
                        case 'copy':
                            break;
                        default:
                            break;
                    }
                }
            }
        });

    })