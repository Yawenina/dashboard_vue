﻿define(['vue', 'text!template/paging/paging.html'], function (Vue, tmpl) {

    return Vue.component('paging', {
        template: tmpl,
        props: {
            effectDatas: {
                type: Array,
                required: true
            },
            currentPageDatas: {     //在使用paging组件的View中绑定一个属性即可，这样翻页的时候不再调用事件
                type: Array,
                twoWay: true,
                required: true
            },
            pageSize: {
                type: Number,
                default: 10
            }
        },
        data: function() {
            return {
                pageIndex: -1
            }
        },
        computed: {
            pageCount: function() {
                return Math.ceil(this.effectDatas.length / this.pageSize);
            },
            pages: function() {
                return new Array(this.pageCount);
            }
        },
        methods: {
            turnToPage: function (pageIndex, isEnforce) {
                if (!isEnforce && pageIndex === this.pageIndex) {
                    return;
                }

                this.pageIndex = pageIndex;
                var start = this.pageIndex * this.pageSize;
                var end = (this.pageIndex + 1) * this.pageSize;                
                this.currentPageDatas = this.effectDatas.slice(start, end);
            }
        },
        events: {
            'paging.turnToPage': function(pageIndex, isEnforce) {
                this.turnToPage(pageIndex, isEnforce);
            }
        }
    });

})