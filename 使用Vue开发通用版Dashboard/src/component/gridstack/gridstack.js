﻿define(['vue', 'text!template/gridstack/gridstack.html',
    'component/gridstack/gridstackItem', 'service/widget', 'service/availableWidget'],
    function (Vue, tmpl, gridstackItemComp, widgetServ, availableWidgetServ) {

        return Vue.component('grid-stack', {
            template: tmpl,
            components: {
                'grid-stack-item': gridstackItemComp
            },
            props: {
                widgets: {
                    type: Array,
                    required: true
                },
                isStaticGrid: {             //dashboardShow会传递是否是固定表格
                    type: Boolean,
                    default: false
                },
                selectFilterItem: {         //dashboardShow会传递选择的过滤项
                    type: Object,
                    default: function() {
                        return {};
                    }
                },   
                dropDown: Function          //dashboardEdit.chartConfigure会传递拖放的回调函数
            },
            data: function () {
                return {
                    config: {
                        defaultLayout: {    //默认按照12x12切割
                            rows: 12,
                            cols: 12,
                            hMargin: 10,
                            vMargin: 10,
                            isFixPosition: true, //是否固定位置（不自动上浮）
                            crossRow: 8,    //widget默认跨几行
                            crossCol: 5     //widget默认跨几列
                        },
                        columnWidth: [] //根据实际的宽度重新计算每种列宽(百分比)，索引0保存的是实际的总宽
                    },
                    isShowConfigure: false,
                    widgetServ: widgetServ,
                    availableWidgetServ: availableWidgetServ
                }
            },
            computed: {
                $element: function () {
                    return $(this.$el).children('.grid-stack');
                },
                gridstackObj: function () {
                    return this.$element.data('gridstack');
                }
            },
            methods: {
                /**
                 * 按照指定行数和列数创建九宫格界面
                 */
                createLayout: function () {

                    var layoutConfig = this.config.defaultLayout;

                    //因为设置的是完整的水平margin，对于每个Component而言，需要设置左右两边的，所以要除以2。
                    //纵向margin是由Gridstack设置的，会自动除以2
                    var margin_h = layoutConfig.hMargin / 2;

                    var bIsFixed = false;
                    //是否固定位置（默认为false，也就是自动排列）
                    if (typeof layoutConfig.isFixPosition === "boolean") {
                        bIsFixed = layoutConfig.isFixPosition;
                    }

                    var $element = this.$element;

                    //计算容器按照指定的行总数和列总数切割后，每个单元格的宽和高
                    var iContainerWidth = $element.width();
                    var iContainerHeight = $element.height();
                    var iOneCellWidth = iContainerWidth;
                    var iOneCellHeight = iContainerHeight;
                    //排除单元格之间的margin，然后再计算每个单元格的宽和高
                    if (layoutConfig.cols > 1) {
                        iOneCellWidth = (iContainerWidth - layoutConfig.hMargin * (layoutConfig.cols - 1)) / layoutConfig.cols;
                    }
                    if (layoutConfig.rows > 1) {
                        iOneCellHeight = (iContainerHeight - layoutConfig.vMargin * (layoutConfig.rows - 1)) / layoutConfig.rows;
                    }

                    //初始化GridStack
                    //因为gridstack未提供横向margin参数，所以需要在初始化gridstack后、以及add_widget之后分别设置
                    $element.gridstack({
                        float: bIsFixed, //float:false——自动排列，true——不自动排列
                        horizontal_margin: margin_h, //horizontal_margin不起作用，只是用来在placeWidget方法中获取横向间距
                        vertical_margin: layoutConfig.vMargin,
                        cell_width: iOneCellWidth,  //cell_width不起作用，只是用来在droppable方法中获取单元格的宽
                        cell_height: 30,//iOneCellHeight,
                        //height: layoutConfig.rows,
                        width: layoutConfig.cols,
                        static_grid: this.isStaticGrid
                    });

                    //gridstack初始化后高度为0（style="height: 0px"），如果采用拖放的方式addWidget无法实现，需要给gridstack设置一个高度
                    $element.height('auto');

                    this.config.columnWidth[0] = iContainerWidth;
                    //如果切割的列数不是12的整数倍(gridstack按照12列切割，在css中对每列的宽度设置了固定比例)，
                    //在添加组件的时候，对组件的包装层需要重新设置css宽度，这里只是做个记录
                    if (layoutConfig.cols % 12 > 0) {
                        //先计算每种列宽对应的宽度(%)，并保存
                        for (var i = 1; i <= layoutConfig.cols; i++) {
                            this.config.columnWidth[i] =
                                iOneCellWidth * i / (iContainerWidth - layoutConfig.hMargin * (layoutConfig.cols - 1)) * 100;
                        }
                    }

                    //如果要调整Gridstack的横向margin，对于拖动框而言，由于拖动框只有一个，所以只需初始化之后设置拖动框的横向margin即可
                    $element.find('.grid-stack-placeholder>.placeholder-content')
                        .css({
                            right: margin_h,
                            left: margin_h
                        });

                    //利用jQuery UI的droppable增加拖放功能
                    $element.droppable({
                        //只有draggable具有相同scope的元素可拖放
                        scope: 'ChartConfigure.WidgetDragAndDrop'
                    });

                    $element
                        .on('resizestop', this, function (event, ui) {
                            /**
                             * 绑定widget的resize事件Handler
                             */
                            var widgetId = $(event.target).attr('widgetId');
                            var widget = event.data.widgetServ.getById(widgetId);
                            event.data.availableWidgetServ.getByName(widget.config.widgetName).chartResize(widget.chartObj);
                        })
                        .on('drop', this, function (event, ui) {
                            /**
                             * 绑定widget的拖放事件Handler
                             */
                            var _this = event.data;

                            //计算位置时要减去gridstack的偏移量
                            var containerOffset = _this.$element.offset();

                            //ui.position保存的是当前拖动的DOM元素与初始位置之间的偏移量
                            //ui.offset保存的是当前拖动的DOM元素与左上角位置之间的偏移量
                            var offsetTop = ui.offset.top - containerOffset.top;
                            //拖动的DOM元素是否超出了有效范围
                            if (offsetTop < 0) {
                                offsetTop = 0;
                            }
                            var offsetLeft = ui.offset.left - containerOffset.left;
                            //拖动的DOM元素是否超出了有效范围
                            if (offsetLeft < 0) {
                                offsetLeft = 0;
                            }

                            //计算位置，并按照小部件配置里的默认布局设置跨多少行多少列
                            var layout = {
                                x: Math.floor(offsetTop / _this.gridstackObj.opts.cell_height),
                                y: Math.floor(offsetLeft / _this.gridstackObj.opts.cell_width),
                                row: _this.config.defaultLayout.crossRow,
                                col: _this.config.defaultLayout.crossCol
                            }

                            //调用拖放的接口，将当前拖动的元素，以及位置作为参数
                            var widget = _this.dropDown(
                                //如果不克隆，则再次拖动此Widget实际拖动的却是Widget列表中的原始Widget
                                ui.draggable.clone(true, false),
                                layout);

                            _this.addWidget(widget);
                        })
                        .on('change', this, function (event, items) {
                            /**
                             * 绑定widget调整大小以及拖放事件Handler
                             */
                            var _this = event.data;
                            for (var i = 0; items && i < items.length; i++) {
                                var $widget = $(items[i].el);
                                var widget = _this.widgetServ.getById($widget.attr('widgetId'));

                                if (!widget) {
                                    continue;
                                }

                                var widgetLayout = widget.config.layout;

                                //因为Gridstack的坐标轴是从左上角开始的，所以比较和保存是x、y要互换

                                //判断当前widget是否挪动位置了，如果挪动了，则改变当前widget的状态
                                if (widget.config.status === 'noModify' &&
                                    (widgetLayout.x !== items[i].y ||
                                    widgetLayout.y !== items[i].x ||
                                    widgetLayout.row !== items[i].height ||
                                    widgetLayout.col !== items[i].width)
                                    ) {

                                    widget.config.status = 'update';
                                }

                                //记录Widget新的位置和大小
                                widgetLayout.x = items[i].y;
                                widgetLayout.y = items[i].x;
                                widgetLayout.row = items[i].height;
                                widgetLayout.col = items[i].width;

                                //调整左右margin
                                _this.calculateMargin(widget);
                            }

                            //当移动/缩放widget后，gridstack的高度会发生变动，
                            //如果gridstack的高度小于放置区域的高度(放置区域有一个最小高度)，则以放置区域的高度作为gridstack的高度
                            if (_this.$element.height() < _this.$element.parent().height()) {
                                _this.$element.height('auto');
                            }
                            

                        });
                },
                calculateMargin: function (widget) {
                    //调整左右margin
                    var widgetLayout = widget.config.layout,
                        margin = this.gridstackObj.opts.horizontal_margin + 'px',
                        marginLeft = widgetLayout.y <= 1 ? 0 : margin,
                        marginRight = widgetLayout.y + widgetLayout.col >= this.config.defaultLayout.cols ? 0 : margin;

                    widgetLayout.marginLeft = marginLeft;
                    widgetLayout.marginRight = marginRight;
                },
                /**
                 * 加载小部件依赖的图表库，并将小部件添加到widget列表中，通过gridstackItem组件将小部件放到界面上
                 * @param widget 要添加的小部件
                 */
                addWidget: function (widget) {

                    var layout = widget.config.layout;

                    //处理widget的位置加上跨行跨列超过切割的总行数或总列数的情况
                    var rowCount = this.config.defaultLayout.rows,
                        colCount = this.config.defaultLayout.cols;
                    layout.x = layout.x + layout.row > rowCount ? rowCount - layout.row : layout.x;
                    layout.y = layout.y + layout.col > colCount ? colCount - layout.col : layout.y;

                    //调整左右margin
                    this.calculateMargin(widget);

                    //if (widget.chartType < 20000000 &&
                    //    !this.availableWidgetServ.getByName(widget.config.widgetName).dependChartModule) {
                    //    console.error('gridstack.addWidget()：widgetId为%d未提供dependChartModule', widget.id);
                    //    return;
                    //}

                    //添加到widgetService中
                    this.widgetServ.add(widget);
                }
            },
            events: {
                'gridstack.removeAllItem': function () {
                    this.gridstackObj.remove_all();
                },
                'gridstack.createLayout': function () {
                    //初始化表格布局
                    this.createLayout();
                }
            },
            ready: function () {
                //如果不是静态表格，表示此时处于编辑状态，因此每个widget需要展示配置按钮
                if (!this.isStaticGrid) {
                    this.isShowConfigure = true;
                }
            }
        });
    }
)