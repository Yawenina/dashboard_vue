﻿define(['vue', 'text!template/gridstack/itemType/daterange.html', 'moment'], function (Vue, tmpl, moment) {

    return Vue.component('daterange', {
        template: tmpl,
        props: {
            widget: {
                type: Object,
                required: true
            },
            isShowConfigure: {
                type: Boolean,
                default: false
            }
        },
        computed: {
            selectFilterCondition: function () {
                var condition = this.widget.config.selectFilterCondition;
                if (!condition) {
                    condition = this.widget.config.selectFilterCondition = [];
                }
                return condition;
            },
            selectDateRange: {
                get: function () {
                    var filterValue = this.selectFilterCondition;

                    //数组中第一个保存起始日期，第二个保存截止日期
                    var returnValue = [];
                    for (var i = 0; i < filterValue.length; i++) {
                        returnValue.push(moment(filterValue[i].value, 'YYYY/MM/DD'));
                    }
                    return returnValue;
                },
                set: function (value) {
                    var setValue = [];
                    for (var i = 0; i < value.length; i++) {
                        setValue.push({
                            key: (i === 0 ? 'startDate' : 'endDate'),
                            value: value[i].format('YYYY/MM/DD')
                        });
                    }
                    this.widget.config.selectFilterCondition = setValue;
                }
            }
        },
        methods: {
            deleteWidget: function () {
                this.$dispatch('gridstackItem.deleteWidget');
            },
            getDateRangePickerOption: function () {
                var dateRanges = [
                    { name: '最近一天', range: -1, unit: 'd' },
                    { name: '最近一周', range: -1, unit: 'w' },
                    { name: '最近一月', range: -1, unit: 'month' },
                    { name: '当前月份', range: 0, unit: 'month' },
                    { name: '最近一年', range: -1, unit: 'y' }
                ];
                var ranges = {};
                for (var j = 0; j < dateRanges.length; j++) {
                    var range = dateRanges[j];
                    if (range.range === 0) {
                        ranges[range.name] = [moment().startOf(range.unit), moment()];
                    }
                    else {
                        ranges[range.name] = [moment().add(range.range, range.unit), moment()];
                    }
                }

                var dateRangePickerOption = {
                    'minDate': '2005/01/01',
                    'maxDate': moment(),
                    //'autoApply': true,      //选完后直接应用，等效于自动点击了apply按钮
                    "showDropdowns": true,
                    "opens": "left",
                    "ranges": ranges,
                    "locale": {
                        "format": "YYYY/MM/DD",
                        "separator": " - ",
                        "applyLabel": "应用",
                        "cancelLabel": "取消",
                        "fromLabel": "起始时间",
                        "toLabel": "截止时间",
                        "customRangeLabel": "自定义",
                        "daysOfWeek": [
                            "周日",
                            "周一",
                            "周二",
                            "周三",
                            "周四",
                            "周五",
                            "周六"
                        ],
                        "monthNames": [
                            "一月",
                            "二月",
                            "三月",
                            "四月",
                            "五月",
                            "六月",
                            "七月",
                            "八月",
                            "九月",
                            "十月",
                            "十一月",
                            "十二月"
                        ],
                        "firstDay": 1
                    }
                };


                if (this.selectDateRange.length < 2) {                                        
                    if (this.selectDateRange.length === 0) {
                        //如果未提供起始日期，默认是最近一个月
                        dateRangePickerOption.startDate = moment().add(-1, 'month');
                        //如果未提供截止日期，默认是今天
                        dateRangePickerOption.endDate = moment();
                    }                    
                    else if (this.selectDateRange.length === 1) {
                        dateRangePickerOption.startDate = this.selectDateRange[0];
                        //如果未提供截止日期，默认是今天
                        dateRangePickerOption.endDate = moment();
                    }
                    //保存日期范围
                    this.selectDateRange = [dateRangePickerOption.startDate, dateRangePickerOption.endDate];
                    if (this.widget.config.status === 'noModify') {
                        this.widget.config.status = 'update';
                    }
                } else {
                    dateRangePickerOption.startDate = this.selectDateRange[0];
                    dateRangePickerOption.endDate = this.selectDateRange[1];
                }

                return dateRangePickerOption;
            },
            initDateRangePicker: function (isShowStatus) {

                var $element = $(this.$el).find('input[name="daterangepicker"]');
                $element.daterangepicker(this.getDateRangePickerOption())
                    .on('apply.daterangepicker', this, function (event, daterangepicker) {
                        var _this = event.data;

                        var startDate = daterangepicker.startDate,
                            endDate = daterangepicker.endDate;
                        //filterKeyName = _this.filterContent.filterKey;

                        /**
                         * 需求变更：如果是月份范围选择，则提交的开始日期为1日，截止日期为月末最后一天
                         * 申请人员：李亚东
                         * 申请日期：2015-09-16
                         */
                        //if (filterKeyName === FilterKeyType[FilterKeyType.MonthPicker]) {
                        //    startDate = startDate.startOf('month');
                        //    endDate = endDate.endOf('month');
                        //}

                        //记录选择的过滤项
                        _this.selectDateRange = [startDate, endDate];

                        //如果不是展示状态，调整日期范围，则认为修改了日期过滤器的默认日期范围，需要保存widget
                        if (!this.isShowStatus && _this.widget.config.status === 'noModify') {
                            _this.widget.config.status = 'update';
                        }

                        //如果是展示状态，dashboardShow小部件是存在的，就能通知刷新数据
                        _this.$dispatch('dashboardShow.filterChange', _this.selectFilterCondition);

                        //保存用户选择的日期范围
                        //if (filterKeyName === FilterKeyType[FilterKeyType.DateTime]) {
                        //    _this.config.groupConfig.dateRange = {
                        //        startDate: daterangepicker.startDate,
                        //        endDate: daterangepicker.endDate
                        //    }
                        //} else {
                        //    _this.config.groupConfig.monthRange = {
                        //        startMonth: daterangepicker.startDate,
                        //        endMonth: daterangepicker.endDate
                        //    }
                        //}
                    }.bind({
                        isShowStatus: isShowStatus
                    }));
            }
        },
        events: {
            'daterange.init': function (isShowStatus) {
                this.initDateRangePicker(isShowStatus);
            }
        } 
    });

})