﻿define(['vue', 'text!template/gridstack/itemType/chart.html', 'service/widget', 'service/availableWidget'],
    function (Vue, tmpl, widgetServ, availableWidgetServ) {

        return Vue.component('chart', {
            template: tmpl,
            props: {
                widget: {
                    type: Object,
                    required: true
                },
                isShowConfigure: {
                    type: Boolean,
                    default: false
                },
                selectFilterItem: {
                    type: Object,
                    default: {}
                }
            },
            data: function () {
                return {
                    widgetServ: widgetServ,
                    availableWidgetServ: availableWidgetServ
                }
            },
            computed: {
                title: function () {
                    return this.widget.config.chartOption.title.text;
                },
                correspondingWidget: function () {
                    return this.availableWidgetServ.getByName(this.widget.config.widgetName);
                }
            },
            methods: {
                openWidgetConfigure: function () {
                    this.$dispatch('gridstackItem.openWidgetConfigure');
                },
                deleteWidget: function () {
                    this.$dispatch('gridstackItem.deleteWidget');
                },
                initChart: function () {

                }
            },
            events: {
                /**
                 * 没有采用ready方法，而采用事件的原因是，图表初始化前一定要先将widget放置到界面上，因为图表需要高度
                 */
                'chart.init': function (isGetData) {
                    //加载依赖的图表模块
                    //require(
                    //    this.correspondingWidget.dependChartModule,
                    //    function () {
                    //        var _this = this.context;

                            var widget = this.widget;

                            var elementSelector = '#widgetId_' + widget.id + ' .widgetContent',
                                $element = $(elementSelector);

                            if ($element.length === 0) {
                                return;
                            }

                            //按照不同图表库初始化图表
                            switch (this.correspondingWidget.dependChartLibrary) {
                                default:    //默认按照echarts初始化图表
                                    widget.chartObj = require('echarts').init($element.get(0));
                                    break;
                            }

                            if (isGetData) {
                                //获取并处理数据，然后绑定到图表上
                                widget.refreshData(this.selectFilterItem);
                            } else {
                                //加载图表模拟数据
                                var availableWidget = this.correspondingWidget;
                                availableWidget.bindData(this.widget, availableWidget.simulateData);
                            }

                            //加载图表的主题
                            var theme = this.correspondingWidget.theme;
                            if (theme) {
                                switch (this.correspondingWidget.dependChartLibrary) {
                                    default:    //默认echarts图表
                                        widget.chartObj.setTheme(theme);
                                        break;
                                }
                            }
                        //}.bind({
                        //    context: this,
                        //    isGetData: isGetData
                        //})
                    //);
                },
                /**
                 * 通过props传递的selectFilterItem，初始化有效。事件里this.selectFilterItem是旧的，所以采用参数的方式获取
                 */
                'chart.refreshChart': function (selectFilterItem) {
                    this.widget.refreshData(selectFilterItem);
                }
            }
        });

    })