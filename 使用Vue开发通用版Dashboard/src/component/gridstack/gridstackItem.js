define(["vue", 'text!template/gridstack/gridstackItem.html', 'service/widget',
    'component/gridstack/itemType/chart', 'component/gridstack/itemType/daterange'],
    function (Vue, tmpl, widgetServ, gridstackItem_chart, gridstackItem_daterange) {

        return Vue.component('grid-stack-item', {
            template: tmpl,
            components: {
                'chart': gridstackItem_chart,
                'daterange': gridstackItem_daterange
            },
            props: {
                widget: {
                    type: Object,
                    required: true
                },
                gridstackObj: {
                    type: Object,
                    required: true
                },
                gridstackConfig: {
                    type: Object,
                    required: true
                },
                isShowConfigure: {
                    type: Boolean,
                    default: false
                },
                selectFilterItem: Object
            },
            data: function () {
                return {
                    widgetServ: widgetServ
                }
            },
            computed: {
                elementId: function () {
                    return 'widgetId_' + this.widget.id;
                },
                gridstackItemContentStyle: function () {
                    return {
                        left: this.widget.config.layout.marginLeft,
                        right: this.widget.config.layout.marginRight
                    }
                },
                gridstackItemTypeName: function () {
                    if (this.widget.chartType < 20000000) {
                        return 'chart';
                    } else if (this.widget.chartType < 30000000) {
                        /**
                         * @todo 需要对不同的过滤器做处理，并不是小于30000000的都是日期过滤器
                         */
                        return 'daterange';
                    }
                }
            },
            methods: {
                /**
                 * 按照指定位置和宽高放置widget
                 * @param $widget 临时生成的widget界面元素
                 */
                placeWidget: function ($widget) {

                    var widgetLayout = this.widget.config.layout,
                        gridstackLayout = this.gridstackConfig.defaultLayout;

                    //验证提交的Widget的位置是否超过UI布局的行和列
                    if (widgetLayout.x > gridstackLayout.rows
                        || widgetLayout.x + widgetLayout.row > gridstackLayout.rows + 1
                        || widgetLayout.y > gridstackLayout.cols
                        || widgetLayout.y + widgetLayout.col > gridstackLayout.cols + 1) {
                        console.error('UILayout.placeComponent('+ $widget.selector +')提供的Widget布局位置不正确');
                        return;
                    }

                    var gridstackObj = this.gridstackObj;

                    //Gridstack的坐标系是从左上角开始的，横向是x，纵向是y，对应的就是x表示处在第几列，y表示处在第几行，所以添加的时候x、y要交换
                    gridstackObj.add_widget($widget,
                        widgetLayout.y, widgetLayout.x,
                        widgetLayout.col, widgetLayout.row);

                    //切割的列数是否是12的整数倍，如果不是，里面会存储从1个列宽到cols个列的每一种列宽
                    var columnWidth = this.gridstackConfig.columnWidth;
                    if (columnWidth[1]) {
                        //如果切割的列数不是12的整数倍，gridstack需要对每个Widget重新设置css宽度
                        $widget.css({
                            //columnWidth[0]里存储的是整个UILayout的宽度，需要进行特殊处理
                            width: (widgetLayout.col < 2 ? columnWidth[1] : columnWidth[widgetLayout.col]) + '%',
                            left: widgetLayout.y === 0 ? 0 : columnWidth[widgetLayout.y] + '%'
                        });
                    }

                    //设置Widget右下角resizable提示的位置
                    $widget.show().find('.ui-resizable-se').css({
                        right: this.gridstackItemContentStyle.right
                    });
                }
            },
            events: {
                'gridstackItem.openWidgetConfigure': function () {
                    this.$dispatch('chartConfigure.widgetConfigure', this.widget);
                },
                'gridstackItem.deleteWidget': function () {
                    //从界面中删除元素
                    var $widget = $('#' + this.elementId);
                    var gridstackObj = this.gridstackObj;
                    gridstackObj.remove_widget($widget);

                    //修改widget的状态
                    this.widget.config.status = 'delete';
                },
                'gridstackItem.placeWidget': function () {
                    this.placeWidget($('#' + this.elementId));
                }
            },
            ready: function () {
                //放置到布局中元素才有高度，所以一定要先placeWidget，后initWidget
                this.$emit('gridstackItem.placeWidget');
                
                switch (this.gridstackItemTypeName) {
                    case 'daterange':
                        //通知日期控件初始化，如果不显示“配置”按钮，需要获取真实数据
                        this.$broadcast('daterange.init', !this.isShowConfigure);
                        break;
                    default:
                        //通知图表widget初始化，如果不显示“配置”按钮，需要获取真实数据
                        this.$broadcast('chart.init', !this.isShowConfigure);
                        break;
                }
            }
        });
    });
