﻿define(['vue', 'text!template/tree/treeItem.html', 'service/metaData'], function (Vue, tmpl, metaDataServ) {

    return Vue.component('tree-item', {
        template: tmpl,
        props: {
            treeData: {
                type: Object,
                required: true
            }
        },
        data: function() {
            return {
                metaDataServ: metaDataServ,
                open: false
            }
        },
        computed: {
            isFolder: function () {
                if (!this.treeData) {
                    return false;
                }
                return this.treeData.children && this.treeData.children.length > 0;
            }
        },
        methods: {
            toggle: function () {
                if (this.isFolder) {
                    this.open = !this.open;
                }
            }
        },
        ready: function () {
            //设置叶子节点的维度指标能够拖放
            $(this.$el).find('.canDragTreeItem').draggable({
                //只有与UILayout的droppable具有相同scope的元素才可拖放
                scope: 'treeItem.widgetDragAndDrop',
                helper: 'clone',
                appendTo: 'body',   //因为有overflow，不把拖动的元素从div中拿出，是拖不出来的
                zIndex: 999999999
            });
        }
    });

})