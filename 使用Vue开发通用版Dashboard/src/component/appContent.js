﻿define(['vue', 'text!template/appContent.html', 'service/widget', 'service/share', 'service/users',
        'component/appContent/dashboardShow', 'component/appContent/dashboardEdit',
        'component/appContent/dashboardShare', 'component/appContent/dashboardEditStep',
        'component/appContent/dashboardEditOperate', 'component/modal', 'component/appContent/groupInfo',
        'component/appContent/purviewManage'
],
    function (Vue, tmpl, widgetServ, shareServ, usersServ, dashboardShowComp, dashboardEditComp, dashboardShareComp,
              dashboardEditStep, dashboardEditOperate, modalComp, groupInfoComp, purviewManageComp) {

        return Vue.component('', {
            template: tmpl,
            data: function () {
                return {
                    widgetServ: widgetServ,
                    shareServ: shareServ,
                    usersServ: usersServ,
                    isShowDashboardEditStep: false,
                    currentView: '',
                    dashboardObjTemp: null,
                    isShowModal: false,
                    loadModalComponentName: '',
                    tempGroup: null
                }
            },
            components: {
                dashboardEditStep: dashboardEditStep,
                dashboardEditOperate: dashboardEditOperate,
                dashboardShow: dashboardShowComp,
                dashboardEdit: dashboardEditComp,
                dashboardShare: dashboardShareComp,
                modal: modalComp,
                groupInfo: groupInfoComp,
                purviewmanage: purviewManageComp
            },
            methods: {
                dashboardShowAndEdit: function (componentName, dashboardObj, options) {

                    //如果没有传递仪表盘，则表示当前分组下没有仪表盘，需要清空界面上已存在的component
                    if (!dashboardObj) {
                        this.currentView = '';
                        //隐藏上部的仪表盘步骤显示状态
                        this.isShowDashboardEditStep = false;

                        //显示左侧的仪表盘列表新建按钮显示状态
                        this.$dispatch('callSiblingEvent', 'dashboardList.hideNewDashboard', false);
                        return false;
                    }

                    //是否隐藏仪表盘编辑状态
                    var isHideEditStatus = true;

                    this.dashboardObjTemp = dashboardObj;
                    //如果是新建或者编辑仪表盘，需要将实际的仪表盘克隆一份，以便能够实现撤销操作
                    if (componentName.indexOf('edit') > 0) {
                        this.dashboardObjTemp = {};
                        //利用jQuery的extend方法，将仪表盘对象克隆一份
                        $.extend(true, this.dashboardObjTemp, dashboardObj);

                        isHideEditStatus = false;
                    }

                    //只有配置当前仪表盘不需要清除原有的widgets，其他情况（切换、新建）都需要清空。避免在初始化界面之前，存在通过编辑界面添加的未保存的widget
                    if (dashboardObj.id > 0 && componentName.indexOf('show') > 0 || dashboardObj.id < 0) {
                        this.widgetServ.delAll();
                    }

                    //切换上部的仪表盘步骤显示状态
                    this.isShowDashboardEditStep = !isHideEditStatus;

                    //切换左侧的仪表盘列表新建按钮显示状态
                    this.$dispatch('callSiblingEvent', 'dashboardList.hideNewDashboard', isHideEditStatus);

                    return true;
                },
                confirmNeedSave: function (models, getStatusFunction) {
                    for (var i = 0; i < models.length; i++) {
                        var status = getStatusFunction(models[i]);
                        if (status !== 'noModify') {
                            return '是否放弃保存对当前功能的所有操作？';                            
                        }
                    }
                    return '';
                }
            },
            events: {
                'appContent.switchView': function (componentName, dashboardObj, options) {

                    var isSwitchView = true;

                    this.isShowDashboardEditStep = false;

                    switch (componentName) {
                        case 'dashboard-share':
                        case 'purview-manage':
                            if (this.currentView === componentName) {
                                return;
                            }
                            break;
                            //case 'dashboard-edit':
                            //    this.isShowDashboardEditStep = true;
                        default:
                            isSwitchView = this.dashboardShowAndEdit(componentName, dashboardObj, options);
                            break;
                    }

                    //是否保存过仪表盘，如果没有保存过，切换显示状态的话，需要有提示
                    if (!options || !options.alreadySave) {
                        var confirmMessage = '';
                        if (this.currentView === 'dashboard-edit' && componentName !== 'dashboard-edit') {
                            confirmMessage = this.confirmNeedSave(this.widgetServ.getAll(), function(widget) {
                                return widget.config.status;
                            });
                        }
                        else if (this.currentView === 'dashboard-share' && componentName !== 'dashboard-share') {
                            confirmMessage = this.confirmNeedSave(this.shareServ.getAll(), function (shareUser) {
                                return shareUser.status;
                            });
                        }
                        else if (this.currentView === 'purview-manage' && componentName !== 'purview-manage') {
                            confirmMessage = this.confirmNeedSave(this.usersServ.getAll(), function (user) {
                                return user.role.status;
                            });
                        }

                        if (confirmMessage.length > 0 && !confirm(confirmMessage)) {
                            return;
                        }
                    }

                    if (isSwitchView) {
                        /**
                        * 如果使用props+生命周期函数的方式，即：只有this.currentView = componentName这一句
                        * 会出现由仪表盘A展示切换到仪表盘B展示时，因为组件已存在，不会再触发create或ready函数的情况
                        * 如果使用props+事件的方式，会出现组件还未初始化完，事件已经广播完了的问题
                        * 
                        * 所以，改成了使用props+生命周期函数与事件方式相结合。
                        * 如果不切换组件，表示组件已初始化过，此时通过事件的方式通知组件
                        * 如果切换组件，通过props+生命周期函数的方式创建组件
                        */
                        if (this.currentView === componentName) {
                            //通过事件传播的方式通知相应widget的switchDashboard事件
                            this.$broadcast(componentName.replace(/-\w/, function (char) {
                                return char.replace('-', '').toUpperCase();
                            }) + '.switchDashboard', this.dashboardObjTemp);
                        } else {
                            //切换到相应的界面组件（展示 or 编辑）
                            this.currentView = componentName;
                        }
                    }
                },
                //'appContent.hideDashboardEditAction': function (isHide) {
                //    if (isHide === true) {
                //        this.isShowDashboardEditStep = false;
                //    }
                //    else {
                //        this.isShowDashboardEditStep = true;
                //    }
                //},
                'appContent.loadModalComponent': function (componentName, group) {
                    if (!componentName) {
                        this.isShowModal = false;
                        return;
                    }

                    this.loadModalComponentName = componentName;
                    this.tempGroup = group;
                    this.isShowModal = true;
                }
            }
        });

    })