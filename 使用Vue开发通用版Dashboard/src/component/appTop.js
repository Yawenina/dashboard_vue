﻿define([
        'vue', 'text!template/appTop.html', 'service/group',
        'component/appTop/dashboardProduct', 'component/appTop/dashboardGroup',
        'component/appTop/userInfo', 'component/appTop/editMenu', 'component/appTop/quit'
],
    function (Vue, tmpl, groupServ,
              dashboardProductComp, dashboardGroupComp, userInfoComp, editMenuComp, quitComp) {

        return Vue.component('app-top', {
            template: tmpl,
            data: function() {
                return {
                    groupServ: groupServ
                }
            },
            components: {
                dashboardProduct: dashboardProductComp,
                dashboardGroup: dashboardGroupComp,
                userInfo: userInfoComp,
                editMenu: editMenuComp,
                quit: quitComp
            },
            methods: {
                createNewGroup: function () {
                    this.$dispatch('callSiblingEvent',
                        'appContent.loadModalComponent', 'group-info', this.groupServ.createNew());
                },
                configureCurrentDashboard: function () {
                    this.$broadcast('editMenu.switchAction', 1);
                }
            }
        });

    })