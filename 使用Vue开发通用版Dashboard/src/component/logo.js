﻿define(['vue', 'service/group'], function (Vue, groupServ) {

    var tmpl = '<div class="logo">{{{logo}}}</div>';

    return Vue.component('logo', {
        template: tmpl,
        data: function () {
            return {
                groupServ: groupServ
            }
        },
        computed: {
            logo: function () {
                var group = this.groupServ.get();
                var logoPath = 'logos/default.png',
                    altValue = '';
                if (group && group.logoPath) {
                    logoPath = group.logoPath;
                    altValue = group.name;
                }

                return '<img width="185" height="63" src="' + logoPath + '" alt="' + altValue + '" />';
            }
        }
    });

})