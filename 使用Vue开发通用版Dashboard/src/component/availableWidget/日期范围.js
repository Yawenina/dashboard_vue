﻿define([''], function() {
    return {
        name: '日期范围',
        chartType: 20010001, //共8位：第1位表示大类（1：chart 2：filter），2~4位表示小类（日期范围选择过滤器:001,下拉菜单过滤器:002,按键助手（搜索提示）:003），5~8位表示序号
        layout: {
            row: 0,
            col: 3
        }
    }
})