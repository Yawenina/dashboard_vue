﻿define(
    [
        'availableWidget/commonFunction/echarts/processData',
        'availableWidget/commonFunction/echarts/bindData',
        'availableWidget/commonFunction/echarts/chartResize'
    ],
    function (processData, bindData, chartResize) {

        /**
         * 给可用widget附加使用的图表库对应的通用功能，如果已存在，则不覆盖，保留原有的
         */
        return function(chart) {
            
            if (!chart.processData) {
                chart.processData = processData;
            }

            if (!chart.bindData) {
                chart.bindData = bindData;
            }

            if (!chart.chartResize) {
                chart.chartResize = chartResize;
            }
        }

    }
)