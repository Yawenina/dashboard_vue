﻿define([''], function () {

    /**
	 * 处理数据
     * @param widget 当前widget
	 * @param arguments 请求返回的数据
	 * @return {JSON} 处理后的数据
	 */
    return function (widget) {
        var returnData = {
            xAxis: [],
            series: []
        };
        
        for (var i = 1; i < arguments.length; i++) {
            if (!arguments[i]) {
                console.error('availableWidget.commonFunction.processData()：widget %d配置的第%d个数据源返回的数据为null', widget.id, i);
                continue;
            }
            var datas = arguments[i].data;
            if (!datas) {
                continue;
            }

            if (!(datas instanceof Array) || typeof datas.length === 'undefined') {
                console.error('availableWidget.commonFunction.processData()：widget %d配置的第%d个数据源返回的数据长度为0', widget.id, i);
                continue;
            }
            for (var j = 0; j < datas.length; j++) {
                var dataIsNull = false,
                    oneData = datas[j];
                //请求数据时columns是按照先selectDimension，然后再selectIndicator，所以这里处理的顺序同请求参数中columns的顺序
                for (var k = 0; k < widget.config.selectDimension.length; k++) {
                    if (!oneData[k]) {
                        dataIsNull = true;
                        break;
                    }
                    if (!returnData.xAxis[k]) {
                        returnData.xAxis[k] = [];
                    }
                    returnData.xAxis[k].push(oneData[k]);
                }
                if (dataIsNull) {
                    continue;
                }

                for (var l = 0; l < widget.config.selectIndicator.length; l++) {
                    if (!returnData.series[l]) {
                        returnData.series[l] = [];
                    }
                    returnData.series[l].push(oneData[k + l]);
                }

            }
        }
        return returnData;
    }

})