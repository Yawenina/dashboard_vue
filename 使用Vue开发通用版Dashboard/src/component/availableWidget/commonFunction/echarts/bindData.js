﻿define([''], function () {

    /**
	 * 将数据绑定到界面
     * @param widget 当前widget
	 * @param data 处理后的数据
	 */
    return function(widget, data) {
        //@todo 现在只有图表，对非图表的数据绑定需要处理
        var chartObj = widget.chartObj,
			chartOption = widget.config.chartOption;

        if (!chartObj || !chartOption || !data) {
            return;
        }

        switch (this.dependChartLibrary) {
            default: //默认按照echarts初始化图表
                var selectPropName = 'selectIndicator';
                //将数据与图表选项结合                
                for (var propName in data) {
                    switch (propName) {
                        case 'xAxis':
                            selectPropName = 'selectDimension';
                            break;
                    }

                    //取图表配置的维度/指标与图表选项中的一个长度
                    var length = widget.config[selectPropName].length || chartOption[propName].length;
                    for (var i = 0; i < length; i++) {
                        if (data[propName].length <= i) {
                            console.error('availableWidget.commonFunction.bindData()：widget %d提供%s数据组数量不够', widget.id, propName);
                            break;
                        }
                        chartOption[propName][i].data = data[propName][i];
                    }

                }

                chartObj.setOption(chartOption);
                break;
        }
    }

})