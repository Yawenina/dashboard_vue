﻿define([''], function () {

    return function(chartObj) {
        //@todo 现在只有图表，对非图表的大小调整需要处理
        if (!chartObj) {
            return;
        }

        //按照不同图表库调整图表大小
        switch (this.dependChartLibrary) {
            default:    //默认按照echarts初始化图表
                chartObj.resize();
                break;
        }
    }
})