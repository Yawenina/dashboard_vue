define(['availableWidget/commonFunction/echarts'],
    function (chartLibCommonFun) {

        var line = {
            name: '曲线图',
            chartType: 10020001, //共8位：第1位表示大类（1：chart 2：filter），2~4位表示小类（001：饼状 002：曲线 003：柱状），5~8位表示序号,
            //dependChartModule: ['echarts/chart/line', 'echarts/chart/bar'],
            chartOption: {
                title: {
                    show: false,
                    text: '未来一周气温变化',
                    subtext: '纯属虚构'
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: ['最高气温', '最低气温']
                },
                toolbox: {
                    show: false,
                    feature: {
                        mark: { show: true },
                        dataView: { show: true, readOnly: false },
                        magicType: { show: true, type: ['line', 'bar'] },
                        restore: { show: true },
                        saveAsImage: { show: true }
                    }
                },
                calculable: true,
                xAxis: [
                    {
                        type: 'category',
                        boundaryGap: false
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        axisLabel: {
                            formatter: '{value} °C'
                        }
                    }
                ],
                series: [
                    {
                        name: '最高气温',
                        type: 'line',
                        markPoint: {
                            data: [
                                { type: 'max', name: '最大值' },
                                { type: 'min', name: '最小值' }
                            ]
                        },
                        markLine: {
                            data: [
                                { type: 'average', name: '平均值' }
                            ]
                        }
                    }
                    //,
                    //{
                    //    name: '最低气温',
                    //    type: 'line',
                    //    markPoint: {
                    //        data: [
                    //            { name: '周最低', value: -2, xAxis: 1, yAxis: -1.5 }
                    //        ]
                    //    },
                    //    markLine: {
                    //        data: [
                    //            { type: 'average', name: '平均值' }
                    //        ]
                    //    }
                    //}
                ]
            },
            simulateData: {
                xAxis: [
                    ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
                ],
                series: [
                    [11, 11, 15, 13, 12, 13, 10]
                    //,[1, -2, 2, 5, 3, 2, 0]
                ]
            }
        }

        //给可用widget附加使用的图表库对应的通用功能
        chartLibCommonFun(line);

        return line;
    }
);
