define(['availableWidget/commonFunction/echarts'],
    function (chartLibCommonFun) {

        var bar = {
            name: '柱状图',
            chartType: 10030001, //共8位：第1位表示大类（1：chart 2：filter），2~4位表示小类（001：饼状 002：曲线 003：柱状），5~8位表示序号
            //dependChartModule: ['echarts/chart/bar'],
            chartOption: {
                title: {
                    show: false,
                    text: '某地区蒸发量和降水量',
                    subtext: '纯属虚构'
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: ['蒸发量', '降水量']
                },
                toolbox: {
                    show: false,
                    feature: {
                        mark: { show: true },
                        dataView: { show: true, readOnly: false },
                        magicType: { show: true, type: ['line', 'bar'] },
                        restore: { show: true },
                        saveAsImage: { show: true }
                    }
                },
                calculable: true,
                xAxis: [
                    {
                        type: 'category'
                    }
                ],
                yAxis: [
                    {
                        type: 'value'
                    }
                ],
                series: [
                    {
                        name: '蒸发量',
                        type: 'bar',
                        markPoint: {
                            data: [
                                { type: 'max', name: '最大值' },
                                { type: 'min', name: '最小值' }
                            ]
                        },
                        markLine: {
                            data: [
                                { type: 'average', name: '平均值' }
                            ]
                        }
                    }
                    //,
                    //{
                    //    name:'降水量',
                    //    type:'bar',
                    //    markPoint : {
                    //        data : [
                    //            {name : '年最高', value : 182.2, xAxis: 7, yAxis: 183, symbolSize:18},
                    //            {name : '年最低', value : 2.3, xAxis: 11, yAxis: 3}
                    //        ]
                    //    },
                    //    markLine : {
                    //        data : [
                    //            {type : 'average', name : '平均值'}
                    //        ]
                    //    }
                    //}
                ]
            },
            simulateData: {
                xAxis: [
                    ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月']
                ],
                series: [
                    [2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3]
                    //,[2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3]
                ]
            }
        }

        //给可用widget附加使用的图表库对应的通用功能
        chartLibCommonFun(bar);

        return bar;
    }
);