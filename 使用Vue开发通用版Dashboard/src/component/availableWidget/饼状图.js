define(['availableWidget/commonFunction/echarts'],
    function (chartLibCommonFun) {

        var pie = {
            name: '饼状图',
            chartType: 10010001, //共8位：第1位表示大类（1：chart 2：filter），2~4位表示小类（001：饼状 002：曲线 003：柱状），5~8位表示序号
            //dependChartModule: ['echarts/chart/pie'],
            isNonRectangular: true,
            chartOption: {
                title: {
                    show: false,
                    text: '某站点用户访问来源',
                    subtext: '纯属虚构',
                    x: 'center'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    show: true,
                    orient: 'horizontal',
                    x: 'left',
                    data: ['直接访问', '邮件营销', '联盟广告', '视频广告', '搜索引擎']
                },
                toolbox: {
                    show: false,
                    feature: {
                        mark: { show: true },
                        dataView: { show: true, readOnly: false },
                        magicType: {
                            show: true,
                            type: ['pie', 'funnel'],
                            option: {
                                funnel: {
                                    x: '25%',
                                    width: '50%',
                                    funnelAlign: 'left',
                                    max: 1548
                                }
                            }
                        },
                        restore: { show: true },
                        saveAsImage: { show: true }
                    }
                },
                calculable: true,
                series: [
                    {
                        name: '访问来源',
                        type: 'pie',
                        radius: '55%',
                        center: ['50%', '60%']
                    }
                ]
            },
            simulateData: {
                series: [
                    [
                        { value: 335, name: '直接访问' },
                        { value: 310, name: '邮件营销' },
                        { value: 234, name: '联盟广告' },
                        { value: 135, name: '视频广告' },
                        { value: 1548, name: '搜索引擎' }
                    ]
                ]
            },
            /**
             * 处理数据
             * @param widget 当前widget
             * @param arguments 请求返回的数据
             * @return {JSON} 处理后的数据
             */
            processData: function(widget) {
                var returnData = {
                    series: []
                };

                for (var i = 1; i < arguments.length; i++) {
                    var datas = arguments[i].data;
                    
                    if (!datas) {
                        continue;
                    }

                    var oneSeries = [];
                    for (var j = 0; j < datas.length; j++) {
                        var oneData = datas[j];

                        //因为饼状图行和列都各只有一个维度/指标，请求的columns是按照先selectDimension，后selectIndicator
                        //所以只取返回的每个数据源里的前两个，第一个作为name，第二个作为value

                        if (!oneData[0]) {
                            continue;
                        }

                        oneSeries.push({
                            name: oneData[0],
                            value: oneData[1]
                        });
                    }
                    //饼状图只有一个series
                    returnData.series.push(oneSeries);
                }

                return returnData;
            }
        }

        //给可用widget附加使用的图表库对应的通用功能
        chartLibCommonFun(pie);

        return pie;
    }
);