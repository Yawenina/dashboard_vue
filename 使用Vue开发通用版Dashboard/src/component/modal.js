﻿define(['vue', 'text!template/modal.html'], function (Vue, tmpl) {

    return Vue.component('modal', {
        template: tmpl,
        props: {
            isShow: {
                type: Boolean,
                required: true
            },
            width: String
        }
    });
})