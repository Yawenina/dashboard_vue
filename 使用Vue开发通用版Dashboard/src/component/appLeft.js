﻿define(['vue', 'text!template/appLeft.html', 'component/appLeft/dashboardList'], function(Vue, tmpl, dashboardList) {

    return Vue.component('app-left', {
        template: tmpl,
        components: {
            'dashboard-list': dashboardList
        }
    });

})