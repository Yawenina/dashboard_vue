﻿define(['vue', 'text!template/appLeft/dashboardList.html', 'service/userInfo', 'service/dashboard'],
    function (Vue, tmpl, userInfoServ, dashboardServ) {

        return Vue.component('dashboard-list', {
            template: tmpl,
            data: function () {
                return {
                    userInfoServ: userInfoServ,
                    dashboardServ: dashboardServ,
                    isShowNewDashboard: true
                }
            },
            computed: {
                dashboards: function () {
                    return this.dashboardServ.getAll();
                }
            },
            methods: {
                isShareDashboard: function (dashboard) {
                    return dashboard.creatorId !== this.userInfoServ.id;
                },
                'isCurrent': function (id) {
                    return id === this.dashboardServ.get().id;
                },
                'dashboardList_OnChange': function (id) {
                    var dashboard = this.dashboardServ.get();
                    if (dashboard) {
                        if (dashboard.id === id) {
                            return;
                        }

                        if (typeof id === 'undefined') {
                            id = dashboard.id;
                        }
                        else if (id > -1) {
                            //设置选择的仪表盘为当前仪表盘
                            this.dashboardServ.setById(id);

                            dashboard = this.dashboardServ.get();
                        }
                    }

                    //是否是新建Dashboard
                    if (id > -1) {
                        //展示当前仪表盘
                        this.$dispatch('callSiblingEvent', 'appContent.switchView', 'dashboard-show', dashboard);
                    }
                    else {
                        //新建仪表盘
                        this.$dispatch('callSiblingEvent', 'editMenu.switchAction', 2, { isDisableChangeGroup: true });
                    }
                }
            },
            events: {
                'dashboardList.groupChange': function (groupId) {
                    //获取指定分组下的所有仪表盘
                    this.dashboardServ.requestAll(groupId).then(function () {
                        //触发dashboardList_OnChange事件
                        if (this.dashboards.length > 0) {
                            this.dashboardList_OnChange();
                        } else {
                            this.dashboardList_OnChange(0);
                        }
                    }.bind(this));
                },
                'dashboardList.hideNewDashboard': function (isHideEditStatus) {
                    this.isShowNewDashboard = !isHideEditStatus;
                }
            }
        });

    })