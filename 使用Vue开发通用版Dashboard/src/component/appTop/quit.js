define(['vue', 'service/userInfo'], function (Vue, userInfoServ) {

    var tmpl = '<button type="button" class="btn btn-default btn-sm" @click="quit">退出</button>';

    return Vue.component('quit', {
        template: tmpl,
        data: function () {
            return {
                userInfoServ: userInfoServ
            }
        },
        methods: {
            quit: function () {
                this.userInfoServ.logout();
            }
        }
    });
});