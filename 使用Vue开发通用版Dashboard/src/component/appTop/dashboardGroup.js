﻿define(['vue', 'text!template/appTop/dashboardGroup.html', 'service/group'], function (Vue, tmpl, groupServ) {

    return Vue.component('dashboard-group', {
        template: tmpl,
        data: function () {
            return {
                groupServ: groupServ
            }
        },
        computed: {
            name: function () {
                var currentGroup = this.groupServ.get();
                return currentGroup && currentGroup.name || "请选择分组";
            },
            groups: function () {
                return this.groupServ.getAll();
            }
        },
        methods: {
            'groupList_OnChange': function (id) {

                if (typeof id === 'undefined') {
                    id = this.groupServ.get().id;
                } else {
                    if (this.groupServ.get().id === id) {
                        return;
                    }
                    //设置选择的分组为当前分组
                    this.groupServ.setById(id);
                }
                //获取指定分组下的全部仪表盘
                this.$dispatch('callSiblingEvent', 'dashboardList.groupChange', id);
            }
        },
        events: {
            'userReady': function () {
                //获取完userInfo以后再获取分组信息
                this.groupServ.requestAll().then(function (groups) {
                    if (groups.length > 0) {
                        //触发groupList_OnChange事件
                        this.groupList_OnChange();
                    }
                }.bind(this));
            }
        }
    });
})