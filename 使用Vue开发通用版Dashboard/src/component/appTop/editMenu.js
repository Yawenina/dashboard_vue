﻿define(
    ['vue', 'text!template/appTop/editMenu.html', 'service/userInfo', 'service/group', 'service/dashboard'],
    function (Vue, editMenuTmpl, userInfoServ, groupServ, dashboardServ) {

        return Vue.component('edit-menu', {
            template: editMenuTmpl,
            data: function () {
                return {
                    userInfoServ: userInfoServ,
                    groupServ: groupServ,
                    dashboardServ: dashboardServ,
                    options: {}
                }
            },
            computed: {
                isShowConfigureMenu: function () {
                    //@todo 如果当前用户不具有编辑、分享权限，是不能够看到配置菜单的
                    return true;
                }
            },
            methods: {
                isShowMenuItem: function (action) {

                    var userInfo = this.userInfoServ;
                    var group = this.groupServ.get();
                    var dashboard = this.dashboardServ.get();

                    switch (action) {
                        case 1:
                            //必须选中一个仪表盘，才能配置当前仪表盘
                            if (dashboard) {
                                /**
                                 * 符合以下任意一条，就能够配置当前仪表盘
                                 * 1、当前用户是管理员（如果用当前用户是否具有编辑权限来判断，会出现给高级用户共享仪表盘，并赋予普通权限，高级用户仍能编辑仪表盘的bug）
                                 * 2、是自己创建的
                                 * 3、共享的仪表盘有编辑权限
                                 */
                                if (userInfo.role.roleType === 1) {
                                    return true;
                                }
                                if (dashboard.creatorId === userInfo.id) {
                                    return true;
                                }
                                if ((dashboard.privilege & 2) === 2) {
                                    return true;
                                }
                            }
                            break;
                        case 2:
                            //必须选中一个分组，才能在这个分组下建仪表盘
                            if (group) {
                                /**
                                 * 符合以下任意一条，就能够新建仪表盘
                                 * 1、当前用户具有新建权限
                                 */
                                if ((userInfo.role.privilege & 8) === 8) {
                                    return true;
                                }
                            }
                            break;
                        case 3:
                            //必须存在分组，才能有共享
                            if (group) {
                                /**
                                 * 符合以下任意一条，就能够共享仪表盘
                                 * 1、当前用户具有共享权限
                                 */
                                if ((userInfo.role.privilege & 4) === 4) {
                                    return true;
                                }
                            }
                            break;
                        case 4:                            
                            /**
                             * 符合以下任意一条，就能够共享仪表盘
                             * 1、当前用户具有管理权限
                             */
                            if ((userInfo.role.privilege & 20) === 20) {
                                return true;
                            }
                            break;
                    }
                    return false;
                },
                switchAction: function (action, option) {
                    //因为配置当前仪表盘有可能通过事件的方式调用，所以在执行的时候判断一下是否有权限
                    if (!this.isShowMenuItem(action)) {
                        return;
                    }

                    switch (action) {
                        case 1:   //执行编辑仪表盘流程                                                        
                            this.$dispatch('callSiblingEvent', 'appContent.switchView', 'dashboard-edit', this.dashboardServ.get());
                            break;
                        case 2:
                            var dashboardObj = this.dashboardServ.createNew(this.groupServ.get().id);
                            dashboardObj.creatorId = this.userInfoServ.id;
                            dashboardObj.creatorEmail = this.userInfoServ.email;

                            //因为通过左侧仪表盘列表添加，分组不能改变也不能新建分组
                            dashboardObj.config.isDisableChangeGroup = option ? option.isDisableChangeGroup : false;
                            this.$dispatch('callSiblingEvent', 'appContent.switchView', 'dashboard-edit', dashboardObj);
                            break;
                        case 3:
                            this.$dispatch('callSiblingEvent', 'appContent.switchView', 'dashboard-share');
                            break;
                        case 4:
                            this.$dispatch('callSiblingEvent', 'appContent.switchView', 'purview-manage');
                            break;
                        default:
                            return;
                    }
                }
            },
            events: {
                /**
                 * 提供事件接口，供其他组件调用配置菜单方法
                 */
                'editMenu.switchAction': function () {
                    this.switchAction.apply(this, arguments);
                }
            }
        });

    })