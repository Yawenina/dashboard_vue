﻿define(['vue','service/userInfo'], function(Vue, userInfoServ) {

    var template = '欢迎您：{{ getName }}';

    return Vue.component('user-info', {
        template: template,
        data: function() {
            return {
                userInfoServ: userInfoServ
            }
        },
        computed: {
            'getName': function () {
                if (this.userInfoServ) {
                    return this.userInfoServ.name || this.userInfoServ.email;
                }
                return "guest";                
            }
        }
    });

})