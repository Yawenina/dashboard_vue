﻿define(['vue', 'text!template/appTop/dashboardProduct.html'], function (Vue, tmpl) {
    return Vue.component('dashboard-product', {
        template: tmpl
    });
})